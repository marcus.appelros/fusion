include("system.jl")
include("utils.jl")

sys=System()
p=Particle(0,0,1,[0,0,0],[0.0,0.0,0])
push!(sys.particles,p)
sys.magneticField=(t,loc)->[0,1,0]
function ef(t,loc)
	if abs(loc[1])>0.1
		return [0,0,0.0]
	end
	q=charge(p)
	Bmag=norm(sys.magneticField(0,[0,0,0]))
	m=mass(p)
	ωc=q*Bmag/m #cyclotron frequency
	return [1*cos(ωc*t),0,0]
end
sys.electricField=ef
n=10000
sys.dt=0.1/n
step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,10)

#lines_in_3D(l0,l1,l2)
