include("system.jl")

function test3(sys3,nstep::Int,sS::Float64)
	
	locs=Vector{Vector{Float64}}()
	vs=Vector{Vector{Float64}}()
	push!(locs,sys3.particles[1].loc)
	push!(vs,sys3.particles[1].v)
	for n in 1:nstep
		if n%10000==0;print(n);print(" ");end
		step!(sys3,true,true)
		push!(locs,sys3.particles[1].loc)
		push!(vs,sys3.particles[1].v)
	end
	return locs,vs
end
nstep=Int(1e4)
sS=0.01
sys3=System([Particle(1,0,0,[0,0,0],[1.0,0.0,0])],sS,x->[0,1,0],x->[1,0,0],0,0,[],[],[],[])
l,vs=test3(sys3,nstep,sS)

l0=Vector{Float64}()
for la in l
	push!(l0,la[1])
end
l1=Vector{Float64}()
for la in l
	push!(l1,la[2])
end
l2=Vector{Float64}()
for la in l
	push!(l2,la[3])
end

using GLMakie
GLMakie.activate!()
function lines_in_3D(x,y,z)
	n=length(x)
	aspect=(1, 1, 1)
	perspectiveness=0.5
	# the figure
	fig = Figure(; size=(1200, 500))
	ax1 = Axis3(fig[1, 1]; aspect, perspectiveness)
	ax2 = Axis3(fig[1, 2]; aspect, perspectiveness)
	ax3 = Axis3(fig[1, 3]; aspect=:data, perspectiveness)
	lines!(ax1, x, y, z; color=1:n, linewidth=3)
	scatterlines!(ax2, x, y, z; markersize=15)
	#hm = meshscatter!(ax3, x, y, z; markersize=0.2, color=1:n)
	hm = meshscatter!(ax3, x, y, z; markersize=0.2, color=sin.(collect(1:n)./10))
	lines!(ax3, x, y, z; color=1:n)
	Colorbar(fig[2, 1], hm; label="values", height=15, vertical=false,
		flipaxis=false, ticksize=15, tickalign=1, width=Relative(3.55/4))
	fig
end

