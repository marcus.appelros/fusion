include("system.jl")
include("utils.jl")

sys=System()
addParticle!(sys,Particle(1,0,0,[0,0,0],[1.0,0.0,0]))
sys.magneticField=(t,loc)->[0,1,0]
sys.electricField=(t,loc)->[0,0,0]
n=100000
sys.dt=10/n
step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,Int(n/1000))

#lines_in_3D(l0,l1,l2)
