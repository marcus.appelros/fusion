include("system.jl")
#include("utils.jl")

sys=System()
n=30
for i in 1:n
	addParticle!(sys,Particle(1,0,0,rand(3).-0.5,rand(3).-0.5))
end
sys.magneticField=(t,loc)->[0,1,0]
sys.electricField=(t,loc)->[0,0,0]
n=1000
sys.dt=0.01
step!(sys,n)

