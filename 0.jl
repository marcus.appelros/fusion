mutable struct Particle
	protons::Int
	neutrons::Int
	electrons::Int
	loc::Vector{Float64}
	v::Vector{Float64}
end
mutable struct System
	particles::Vector{Particle}
	stepSize::Float64 #in units of time
	time::Float64
end
System(a)=System(a,0.1,0)
System(a,sS)=System(a,sS,0)

function charge(p::Particle)
	return p.protons-p.electrons
end
#proton mass=1.67262189821×10^−24 g -> 1
#neutron mass=1.67492749804×10^−24 g -> 1.0013784345598173
#electron mass=9.1093837015×10^−28 g -> 0.0005446170297811266
function mass(p::Particle)			#TODO benchmark ::FLoat64
	return p.protons+p.neutrons*1.0013784345598173+p.electrons*0.0005446170297811266			#TODO mass defect, better to have individual H,D,T,He?
end

#F=k*Q1*Q2/d^2			https://www.physicsclassroom.com/class/estatics/Lesson-3/Coulomb-s-Law	
#a=F/m
function distance(p0::Particle,p1::Particle)
	d=0.0
	@simd for i in eachindex(p0.loc)			#TODO Benchmark @simd difference
		d+=abs(p0.loc[i] - p1.loc[i])
	end
	return d
end
function mag(v::Vector) #magnitude
	sqs=0
	for i in eachindex(v)
		sqs+=v[i]^2
	end
	return sqrt(sqs)
end
function diruv(v0::Vector,v1::Vector) #directional unit vector
	v=v1-v0
	return v/mag(v)
end
function forceCoulomb(p0::Particle,p1::Particle) #force on p0			TODO optimmize for allocations, preallocate?
	q0=charge(p0)
	q1=charge(p1)
	d=distance(p0,p1)
	F=q0*q1/d^2 #k=1, Coulomb's law constant
	Fv=-diruv(p0.loc,p1.loc)*F
	return Fv
end
function forcesCoulomb(s::System)
	np=length(s.particles)
	forces=Vector{Vector{Float64}}(undef,np)
	fill!(forces,zeros(length(s.particles[1].v)))			#TODO benchmark hardcoding this to 3. LINK 0
	for i in 1:np
		for j in 1:np
			if i==j
				continue
			end
			Fv=forceCoulomb(s.particles[i],s.particles[j])
			forces[i]+=Fv
			forces[j]-=Fv
		end
	end	
	return forces
end
function f2a(s::System,forces::Vector)			#TODx benchmark baking this into forcesCoulomb. No we need it for other forces
	np=length(s.particles)
	a=Vector{Vector{Float64}}(undef,np)
	fill!(a,zeros(length(s.particles[1].v)))			#LINK 0, using zeros instead of undef doesn't seem to have a penalty, but it should...
	for i in 1:np
		a[i]=forces[i]/mass(s.particles[i])
	end
	return a
end


function step0!(s::System)
	for p in s.particles
		if p.v==[0.0,0.0,0.0]
			continue
		end
		d=p.v*s.stepSize
		p.loc+=d
	end
	s.time+=s.stepSize
	return s
end
function test0()
	p=Particle(1,0,0,[0,0,0],[1,0,0])
	sys0=System([p])
	step0!(sys0)
end	



function step1!(s::System)
	a=f2a(s,forcesCoulomb(s))
	for i in eachindex(a)
		s.particles[i].v+=a[i]*s.stepSize
	end
	for p in s.particles
		if p.v==[0.0,0.0,0.0]
			continue
		end
		d=p.v*s.stepSize
		p.loc+=d
	end
	s.time+=s.stepSize
	return s
end
function test1()
	sys1=System([Particle(1,0,0,[-1,0,0],[0,0,0]),Particle(1,0,0,[1,0,0],[0,0,0])])
	step1!(sys1)
	println(sys1)
	push!(sys1.particles,Particle(0,0,1,[0,1,0],[0,0,0]))
	step1!(sys1)
	println(sys1)
end


#Lorentz force: qvxB
using LinearAlgebra
magneticField(loc)=[0,1,0] #constant field everywhere, bake into system struct		TODO find out if a stationary proton experiences force from a B gradient
function cvv(nv,n) #create vector of vectors			TODO benchmark difference with pushing
	vv=Vector{Vector{Float64}}(undef,nv)
	fill!(vv,zeros(n))			#LINK 0. Fatal, apparently this fills the vector with the same vector
	return vv
end
function forcesLorentz(s::System)
	np=length(s.particles)
#	forces=Vector{Vector{Float64}}(undef,np)
#	fill!(forces,zeros(length(s.particles[1].v)))			#LINK 0
	#forces=cvv(np,length(s.particles[1].v))
	forces=Vector()
	for i in 1:np
		p=s.particles[i]			#TODx benchmark difference between assigning and looking up. Looking up seems marginally faster
		q=charge(p)
		B=magneticField(p.loc)
		#forces[i]=q*cross(p.v,B)			#TODx benchmark LA cross vs custom built, apparently norm was slow. Norm wasn't slow.
		push!(forces,q*cross(p.v,B))
	end
	return forces
end
#using Roots
function step2!(s::System)
	a=f2a(s,forcesLorentz(s))
	for i in eachindex(a)
	#= Scale Before
		d=a[i]*s.stepSize
		x=find_zero((x)->mag(s.particles[i].v/x+d)-mag(s.particles[i].v),1.005)
		s.particles[i].v/=x
		s.particles[i].v+=d
	=# # Scale After
		m=mag(s.particles[i].v)
		s.particles[i].v+=a[i]*s.stepSize
		x=mag(s.particles[i].v)/m
		s.particles[i].v/=x
	
	end # the results are identical
	for p in s.particles
		if p.v==[0.0,0.0,0.0] #remove? Particles realistically never have 0 speed.
			continue
		end
		d=p.v*s.stepSize
		p.loc+=d
	end
	s.time+=s.stepSize
	return s
end
function test2(nstep,sS)
	sys2=System([Particle(1,0,0,[0,0,0],[1,0,0])],sS)
	#locs=cvv(nstep,3)
	locs=Vector{Vector{Float64}}()
	vs=Vector{Vector{Float64}}()
	push!(locs,sys2.particles[1].loc)
	push!(vs,sys2.particles[1].v)
	for n in 1:nstep
		if n%10000==0;print(n);print(" ");end
		step2!(sys2)
		#locs[n][:]=sys2.particles[1].loc[:]			#TODO benchmark variants of this
		push!(locs,sys2.particles[1].loc)
		push!(vs,sys2.particles[1].v)
	end
	return locs,vs
end
nstep=1000
sS=0.01
l,vs=test2(nstep,sS)
ml=mag.(l)
mv=mag.(vs)
#using Plots
#plot(m)
