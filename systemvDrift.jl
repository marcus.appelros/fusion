mutable struct Particle
	protons::Int
	neutrons::Int
	electrons::Int
	loc::Vector{Float64}
	v::Vector{Float64}
	vDrift::Vector{Float64}
	vBoris::Vector{Float64}
end
Particle(p,n,e,l,v)=Particle(p,n,e,l,v,[0.0,0,0],v)
mutable struct System
	particles::Vector{Particle}
	dt::Float64
	magneticField
	electricField
	t::Float64
	step::Int
	tHist::Vector{Float64}
	locHist::Vector
	vHist::Vector
	aHist::Vector
end
System()=System([],0.01,0,0,0.0,1,[],[],[],[])

function addParticle!(s::System,p::Particle)
	if isempty(s.particles)
		push!(s.locHist,[])
		push!(s.vHist,[])
	else
		reset!(s)
	end
	push!(s.particles,p)
	push!(s.locHist[1],p.loc)
	push!(s.vHist[1],p.v)
	return s
end

include("physics.jl")

function methodBoris(s::System)
	vv=Vector()
	for p in s.particles
		m=mass(p)
		q=charge(p)
		vB=p.vBoris
		vAc=1
		B=s.magneticField(s.t,p.loc)
		E=s.electricField(s.t,p.loc)
		t = q / m * B * 0.5 * s.dt;
		u = 2. * t ./ (1. .+ t.*t); #renamed from s
		v_minus = vB + q / (m * vAc) * E * 0.5 * s.dt;
		v_prime = v_minus + cross(v_minus,t);
		v_plus = v_minus + cross(v_prime,u);
		v = v_plus + q / (m * vAc) * E * 0.5 * s.dt;
		push!(vv,v)
	end
	return vv	
end

function step!(s::System,saveHist::Bool=true)
	s.step+=1
	if saveHist
		push!(s.locHist,[])
		push!(s.vHist,[])
		#push!(s.aHist,[])
	end
	a=f2a(s,forcesParallelMagneticDrift(s))
	for i in eachindex(s.particles)
		s.particles[i].vDrift=s.particles[i].vDrift+a[i]*s.dt
	end
	vv=methodBoris(s)	
	for i in eachindex(s.particles)
		s.particles[i].vBoris=vv[i]
	end
	for i in eachindex(s.particles)
		s.particles[i].v=s.particles[i].vDrift+s.particles[i].vBoris
		s.particles[i].loc=s.particles[i].loc+s.particles[i].v*s.dt
	end
	s.t+=s.dt
	if saveHist
		push!(s.tHist,s.t)
		for i in eachindex(s.particles)
			push!(s.vHist[end],s.particles[i].v)
			push!(s.locHist[end],s.particles[i].loc)
		end
	end
	return s
end
function step!(s::System,n::Int,saveEvery::Int=1,prog::Int=150000)
	for step in 1:n
		if step%prog==0
			print(100*step/n,"% done. ")
		end
		if step%saveEvery==0
			step!(s)
		else
			step!(s,false)
		end
	end
	return s
end
function reset!(s::System)			#TODO add initial values to hist
	if isempty(s.particles)
		return s
	end
	s.t=0
	s.step=1
	s.tHist=[]
	il=s.locHist[1]
	s.locHist=[]
	iv=s.vHist[1]
	s.vHist=[]
	s.aHist=[]
	for i in eachindex(s.particles)
		s.particles[i].loc=il[i]
		s.particles[i].v=iv[i]
	end
	push!(s.tHist,s.t)
	push!(s.locHist,il)
	push!(s.vHist,iv)
	return s
end
