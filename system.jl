include("components.jl")

mutable struct Particle
	protons::Int
	neutrons::Int
	electrons::Int
	loc::Vector{Float64}
	v::Vector{Float64}
end
mutable struct System
	particles::Vector{Particle}
	dt::Float64
	magneticField::Function
	electricField::Function
	components::Array
	t::Float64
	step::Int
	tHist::Vector{Float64}
	locHist::Vector
	vHist::Vector
	aHist::Vector
	stuck::Vector{Int} #a vector with the step at which a particle became stuck
	allStuck::Bool
end
System()=System([],0.01,(t,loc)->[0,0,0],(t,loc)->[0,0,0],[],0.0,1,[],[],[],[],[],false)

include("physics.jl")

function addParticle!(s::System,p::Particle)
	if isempty(s.particles)
		push!(s.locHist,[])
		push!(s.vHist,[])
	else
		reset!(s)
	end
	push!(s.particles,p)
	push!(s.locHist[1],p.loc)
	push!(s.vHist[1],p.v)
	push!(s.stuck,0)
	return s
end


function step!(s::System,saveHist::Bool=true)
	if saveHist
		push!(s.locHist,[])
		push!(s.vHist,[])
		#push!(s.aHist,[])
	end
	#=a=f2a(s,forcesParallelMagneticDrift(s))
	for i in eachindex(s.particles)
		s.particles[i].v=s.particles[i].v+a[i]*s.dt
	end
	=#
	vv=methodBoris(s)	
	for i in eachindex(s.particles)
		s.particles[i].v=vv[i]
		d=vv[i]*s.dt
		s.particles[i].loc=s.particles[i].loc+d
	end
	s.t+=s.dt
	if saveHist
		push!(s.tHist,s.t)
		for i in eachindex(s.particles)
			push!(s.vHist[end],s.particles[i].v)
			push!(s.locHist[end],s.particles[i].loc)
		end
	end
	s.step+=1
	return s
end
function step!(s::System,n::Int,saveEvery::Int=1,prog::Int=150000)
	for step in 1:n
		if step%prog==0
			print(100*step/n,"% done. ")
		end
		if step%saveEvery==0
			step!(s)
		else
			step!(s,false)
		end
		if s.allStuck
			break
		end
	end
	return s
end
function reset!(s::System)			#TODO add initial values to hist
	if isempty(s.particles)
		return s
	end
	s.t=0
	s.step=1
	s.tHist=[]
	il=s.locHist[1]
	s.locHist=[]
	iv=s.vHist[1]
	s.vHist=[]
	s.aHist=[]
	for i in eachindex(s.particles)
		s.particles[i].loc=il[i]
		s.particles[i].v=iv[i]
	end
	push!(s.tHist,s.t)
	push!(s.locHist,il)
	push!(s.vHist,iv)
	return s
end
