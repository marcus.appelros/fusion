include("system.jl")

sys=System()
push!(sys.particles,Particle(1,0,0,[0,0,0],[1.0,0.0,0]))
sys.magneticField=x->[0,1,0]
sys.electricField=x->[0,0,0]
step!(sys,false)
step!(sys,false)

using Interpolations

asp1=[sys.aHist[end-1][1],sys.aHist[end][1]]

ai=linear_interpolation(collect(1:length(asp1)),asp1,extrapolation_bc = Line())

amp=aMidpoint(sys)

#=
step!(sys)

asp1full=[sys.aHist[1][1],sys.aHist[2][1],sys.aHist[3][1]]
asp1end=[sys.aHist[2][1],sys.aHist[3][1]]
aifull=linear_interpolation(collect(1:length(asp1full)),asp1full,extrapolation_bc = Line())
aiend=linear_interpolation(collect(1:length(asp1end)),asp1end,extrapolation_bc = Line())
=#
