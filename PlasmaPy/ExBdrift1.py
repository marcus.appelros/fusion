import math

import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np

from plasmapy import formulary, particles


# Initialize proton in uniform B field
B = 5 * u.T
proton = particles.Particle("p+")
omega_c = formulary.frequencies.gyrofrequency(B, proton)
v_perp = 0.03 * u.m / u.s
r_L = formulary.lengths.gyroradius(B, proton, Vperp=v_perp)


def single_particle_trajectory(v_d=np.array([0, 0, 0])):
    # Set time resolution & velocity such that proton goes 1 meter along B per rotation
    T = 2 * math.pi / omega_c.value  # rotation period
    v_parallel = 0 / T * u.m / u.s
    dt = T / 1e2 * u.s

    # Set initial particle position
    x = []
    y = []
    xt = 0 * u.m
    yt = -r_L

    # Evolve motion
    timesteps = np.arange(0, 100 * T, dt.value)
    for t in list(timesteps):
        v_x = v_perp * math.cos(omega_c.value * t) + v_d[0]
        v_y = v_perp * math.sin(omega_c.value * t) + v_d[1]
        xt += +v_x * dt
        yt += +v_y * dt
        x.append(xt.value)
        y.append(yt.value)
    x = np.array(x)
    y = np.array(y)
    z = v_parallel.value * timesteps

    return x, y, z
    
x, y, z = single_particle_trajectory()

fig = plt.figure(figsize=(6, 6))
ax = fig.add_subplot(111, projection="3d")
ax.plot(x, y, z, label=r"$\mathbf{F}=0$")
ax.legend()
bound = 3 * r_L.value
ax.set_xlim([-bound, bound])
ax.set_ylim([-bound, bound])
ax.set_zlim([0, 10])
ax.set_xlabel("x [m]")
ax.set_ylabel("y [m]")
ax.set_zlabel("z [m]")
#plt.show()


print(f"r_L = {r_L.value:.2e} m")
print(f"omega_c = {omega_c.value:.2e} rads/s")


E = 0.2 * u.V / u.m  # E-field magnitude
ey = np.array([0, 1, 0])
ez = np.array([0, 0, 1])
F = proton.charge * E  # force due to E-field

v_d = formulary.drifts.force_drift(F * ey, B * ez, proton.charge)
print("F drift velocity: ", v_d)
v_d = formulary.drifts.ExB_drift(E * ey, B * ez)
print("ExB drift velocity: ", v_d)


x_d, y_d, z_d = single_particle_trajectory(v_d=v_d)


fig = plt.figure(figsize=(6, 6))
ax = fig.add_subplot(111, projection="3d")
ax.plot(x, y, z, label=r"$\mathbf{F}=0$")
ax.plot(x_d, y_d, z_d, label=r"$\mathbf{F}=q\mathbf{E}$")

bound = 3 * r_L.value
ax.set_xlim([-bound, bound])
ax.set_ylim([-bound, bound])
ax.set_zlim([0, 10])
ax.set_xlabel("x [m]")
ax.set_ylabel("y [m]")
ax.set_zlabel("z [m]")
ax.legend()
plt.show()


print(f"r_L = {r_L.value:.2e} m")
print(f"omega_c = {omega_c.value:.2e} rads/s")
