using ComponentArrays, Lux, DiffEqFlux, OrdinaryDiffEq, Optimization, OptimizationOptimJL,
      OptimizationOptimisers, Random, Plots

rng = Xoshiro(0)
u0 = Float32[2.0; 0.0]
datasize = 30
tspan = (0.0f0, 1.5f0)
tsteps = range(tspan[1], tspan[2]; length = datasize)

function trueODEfunc(du, u, p, t)
    true_A = [-0.1 2.0; -2.0 -0.1]
    du .= ((u .^ 3)'true_A)'
end

prob_trueode = ODEProblem(trueODEfunc, u0, tspan)
ode_data = Array(solve(prob_trueode, Tsit5(); saveat = tsteps))
prob_trueode2 = ODEProblem(trueODEfunc, u0.+0.1, tspan)
ode_data2 = Array(solve(prob_trueode2, Tsit5(); saveat = tsteps))


dudt2 = Chain(x -> x .^ 3, Dense(2, 50, tanh), Dense(50, 2))
dudt2print = Chain(x -> begin;print("x: ",x,". tODE: ",trueODEfunc([0.0,0],x,0,0));return x .^ 3;end, Dense(2, 50, tanh), Dense(50, 2,o->begin;println(". o: ",o);return o;end))
p, st = Lux.setup(rng, dudt2)
prob_neuralode = NeuralODE(dudt2, tspan, Tsit5(); saveat = tsteps)
prob_neuralodeprint = NeuralODE(dudt2print, tspan, Tsit5(); saveat = tsteps)

function predict_neuralode(p)
    Array(prob_neuralode(u0, p, st)[1])
end

function loss_neuralode(p)
    pred = predict_neuralode(p)
    loss = sum(abs2, ode_data .- pred)
    return loss, pred
end

# Do not plot by default for the documentation
# Users should change doplot=true to see the plots callbacks
callback = function (p, l, pred; doplot = false)
    println(l)
    # plot current prediction against data
    if doplot
        plt = scatter(tsteps, ode_data[1, :]; label = "data")
        scatter!(plt, tsteps, pred[1, :]; label = "prediction")
        display(plot(plt))
    end
    return false
end

pinit = ComponentArray(p)
callback(pinit, loss_neuralode(pinit)...; doplot = true)

# use Optimization.jl to solve the problem
adtype = Optimization.AutoZygote()

optf = Optimization.OptimizationFunction((x, p) -> loss_neuralode(x), adtype)
optprob = Optimization.OptimizationProblem(optf, pinit)

result_neuralode = Optimization.solve(
    optprob, OptimizationOptimisers.Adam(0.05); callback = callback, maxiters = 300)

optprob2 = remake(optprob; u0 = result_neuralode.u)

result_neuralode2 = Optimization.solve(
    optprob2, Optim.BFGS(; initial_stepnorm = 0.01); callback, allow_f_increases = false)

callback(result_neuralode2.u, loss_neuralode(result_neuralode2.u)...; doplot = true)


trueODEfunc([0.0,0],u0,1,1)
p1=result_neuralode2.u
dudt2(u0,p1,st)[1]

dudt(u, p, t) = dudt2(u, p, st)[1]
prob_model = ODEProblem(dudt, u0, tspan, p1)
model_data = Array(solve(prob_model, Tsit5(); saveat = tsteps))
pred=Array(prob_neuralode(u0,p1,st)[1])
