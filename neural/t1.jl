import Pkg
Pkg.activate(homedir()*"/julia/Plasmajl")
include(homedir()*"/.julia/dev/Plasma/src/Plasma.jl")
using .Plasma
using DomainSets,LinearAlgebra
using ModelingToolkit#,DifferentialEquations
using Lux
using NeuralPDE

function divergence(Ds, f, v=ones(length(Ds)))
    if f isa AbstractArray
        sum([v[i] * Ds[i](f[i]) for i in eachindex(Ds)])
    elseif Ds isa AbstractArray
        sum([v[i] * Ds[i](f) for i in eachindex(Ds)])
    else
    	sum(v*Ds(f))
    end
end
function curl(vec, Ds)
    [Ds[2](vec[3]) - Ds[3](vec[2]), Ds[3](vec[1]) - Ds[1](vec[3]), Ds[1](vec[2]) - Ds[2](vec[1])]
end
#=import .Plasma.Maxwellian
function Maxwellian(T, m; v_drift=zeros())
        function P(x,v) 
            if !(v isa Array)
                v = [v]    
            end

            if v_drift == zeros()
                v_drift = zeros(length(v))
            end
		println(v)
		println(v_drift)
            if length(v) != length(v_drift)
                error("v and v_drift should have the same length")
            end

            v_ = sqrt(sum(v .^2))
            v_drift_ = sqrt(sum(v_drift.^2))

            1/(2*π*T) * exp(-(v_ - v_drift_)^2/(2*T))
        end
end
=#
#TD = 0.3 # eV
Te = 10_000 # 0.2 # eV
#D = species.D
e = species.e

#D_D = Distribution(Maxwellian(TD, D.m), D)
D_e = Distribution(Maxwellian(Te, e.m), e) 
G = Geometry() 

#plasma = ElectrostaticPlasma([D_D, D_e], G)
plasma = CollisionlessPlasma([D_e], G)

lb=0.0;ub=1.0;time_lb=lb;time_ub=ub

dim = 3
geometry = plasma.geometry.f # this might change with a geometry refactor
dis = plasma.distributions
#P=dis[1].P
speci = [d.species for d in dis]
consts = Constants()
μ_0, ϵ_0 = consts.μ_0, consts.ϵ_0

qs, ms = Float64[], Float64[]
for s in speci
push!(qs,s.q)
push!(ms,s.m)
end
Ps = [d.P for d in dis]

normalized=true
if normalized 
	qs .= 1
	ms .= 1
	ϵ_0 = 1
	μ_0 = 0
end

# variables
fs = Symbolics.variables(:f, eachindex(speci); T=SymbolicUtils.FnType{Tuple,Real})
Es = Symbolics.variables(:E, 1:dim; T=SymbolicUtils.FnType{Tuple,Real})
Bs = Symbolics.variables(:B, 1:dim; T=SymbolicUtils.FnType{Tuple,Real})

# parameters
@parameters t
xs,vs = Symbolics.variables(:x, 1:dim), Symbolics.variables(:v, 1:dim)

# integrals
_I = Integral(tuple(vs...) in DomainSets.ProductDomain(ClosedInterval(-Inf ,Inf), ClosedInterval(-Inf ,Inf), ClosedInterval(-Inf ,Inf)))

# differentials
Dxs = Differential.(xs)
Dvs = Differential.(vs)
Dt = Differential(t)

# domains
xs_int = xs .∈ Interval(lb, ub)
vs_int = vs .∈ Interval(lb, ub)
t_int = t ∈ Interval(time_lb, time_ub)

domains = [t_int;xs_int;vs_int]

# helpers
_Es = [E(t,xs...) for E in Es]
_Bs = [B(t,xs...) for B in Bs]
_fs = [f(t,xs...,vs...) for f in fs]

# divergences
div_vs = [divergence(Dxs, _f, vs) for _f in _fs]
div_B = divergence(Dxs, _Bs)
div_E = divergence(Dxs, _Es)
Fs = [qs[i]/ms[i] * (_Es + cross(vs,_Bs)) for i in eachindex(qs)]
divv_Fs = [divergence(Dvs, _fs[i], Fs[i]) for i in eachindex(_fs)]

# charge and current densities
ρ = sum([qs[i] * _I(_fs[i]) for i in eachindex(qs)])
J = sum([qs[i] * _I(_fs[i]) * vs[j] for i in 1:length(_fs), j in 1:length(vs)]) 

# system of equations
vlasov_eqs = Dt.(_fs) .~ .- div_vs .- divv_Fs
curl_E_eqs = curl(_Es, Dxs) .~ Dt.(_Bs)
curl_B_eqs = ϵ_0*μ_0 * Dt.(_Es) .- curl(_Bs, Dxs) .~ - μ_0.*J
div_E_eq = div_E ~ ρ/ϵ_0
div_B_eq = div_B ~ 0
eqs = [vlasov_eqs; curl_E_eqs; curl_B_eqs; div_E_eq; div_B_eq]

# boundary conditions
div_E0 = sum([Dxs[i](Es[i](time_lb, xs...)) for i in eachindex(Dxs)])
div_B0 = sum([Dxs[i](Bs[i](time_lb, xs...)) for i in eachindex(Dxs)])

vlasov_ics = [fs[i](time_lb,xs...,vs...) ~ Ps[i](xs,vs) * geometry(xs) for i in eachindex(fs)]
div_B_ic = div_B0 ~ 0
div_E_ic =  div_E0 ~ sum([qs[i] * _I(fs[i](time_lb,xs...,vs...)) for i in eachindex(qs)])/ϵ_0 * geometry(xs)
E_bcs=Neumann
f_bcs=(a=1, g=0)
E_bcs = [E_bcs(t, xs, Dxs, Es, lb); E_bcs(t, xs, Dxs, Es, ub)]
f_bcs = [Reflective(t, xs, vs, fs, lb, f_bcs.a, f_bcs.g); Reflective(t, xs, vs, fs, ub, f_bcs.a, f_bcs.g)]
bcs = [vlasov_ics;div_B_ic; div_E_ic; E_bcs; f_bcs]

# get variables
vars_arg = [_fs...; _Es...; _Bs...]
vars = [fs, Bs, Es]

dict_vars = Dict()
for var in vars
push!(dict_vars, var => [v for v in var])    
end

# set up PDE System
@named pde_system = PDESystem(eqs, bcs, domains, [t,xs...,vs...], vars_arg)
#@mtkbuild sys = PDESystem(eqs, [t,xs...,vs...])

#strat=Plasma.NeuralPDE.QuadratureTraining()
#strat=Plasma.NeuralPDE.QuadratureTraining(maxiters=1,batch=1)
#sol = Plasma.solve(plasma, dim=1, GPU=false, strategy=strat)


dim1 = 1
@parameters x1 v1
P1=Plasma.Maxvellian(1,1)
Dx1 = Differential(x1)
Dv1 = Differential(v1)
x1_int = x1 ∈ Interval(lb, ub)
v1_int = v1 ∈ Interval(lb, ub)
domains1 = [t_int;x1_int;v1_int]
f1=Symbolics.variable(:f;T=SymbolicUtils.FnType{Tuple,Real})
_f1=f1(t,x1,v1)
div_v1 = divergence(Dx1, _f1, v1)
vlasov_eq1 = Dt(_f1) ~ - div_v1
vlasov_ic1 = f1(time_lb,x1,v1) ~ P1(x1,v1)
#f_bc1=(a=1, g=0)
#f_bc1 = [Reflective(t, x1, v1, f1, lb, f_bc1.a, f_bc1.g); Reflective(t, x1, v1, f1, ub, f_bc1.a, f_bc1.g)]
f_bc1 = f1(t,x1,1)~f1(t,x1,-1)
bc1=[vlasov_ic1;f_bc1]
vars_arg1 = [_f1]
@named sys1 = PDESystem(vlasov_eq1, bc1, domains1, [t,x1,v1], vars_arg1)

#strat=Plasma.NeuralPDE.QuadratureTraining(maxiters=1,batch=1)
strat=Plasma.NeuralPDE.QuadratureTraining()

il=33
chain=Lux.Chain(Lux.Dense(3, il, sigmoid_fast), Lux.Dense(il, il, sigmoid_fast), Lux.Dense(il, 1))

discretization = NeuralPDE.PhysicsInformedNN(chain, strat)
prob = SciMLBase.discretize(sys1, discretization)

i=0
stopat=1e-3
cb = function (p, l)
	global i
	i+=1
    println(i,": Current loss is: $l")
    if l<stopat
    	return true
    end
    return false
end

opt=Plasma.ADAM(0.01)
res = Plasma.Optimization.solve(prob, opt, callback = cb, maxiters=1000, maxtime=1.0)

prob = remake(prob, u0 = res.minimizer)
res = Optimization.solve(prob, ADAM(0.01); callback = callback, maxiters = 2000)

phi = discretization.phi
phi([0, 0, 0], res.minimizer)
