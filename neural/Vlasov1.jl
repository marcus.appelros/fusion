using DomainSets,LinearAlgebra
using ModelingToolkit
using Lux
using NeuralPDE
using Optimization
#using OptimizationOptimJL
using OptimizationOptimisers

function divergence(Ds, f, v=ones(length(Ds)))
	if v isa Number
		v=ones(length(Ds))*v
	end
	if f isa AbstractArray
		sum([v[i] * Ds[i](f[i]) for i in eachindex(Ds)])
	elseif Ds isa AbstractArray
		sum([v[i] * Ds[i](f) for i in eachindex(Ds)])
	else
		sum(v*Ds(f))
	end
end
function curl(vec, Ds)
    [Ds[2](vec[3]) - Ds[3](vec[2]), Ds[3](vec[1]) - Ds[1](vec[3]), Ds[1](vec[2]) - Ds[2](vec[1])]
end
function Neumann(t, xs, vs, Ds, Fs, bc; bc_value = 0)
    bcs = []
    for F in Fs
        for i in eachindex(xs)
            temp = xs[i]
            xs[i] = bc
            push!(bcs,Ds[i](F(t, xs..., vs...)) ~ bc_value)
            xs[i] = temp
        end    
    end
    
    return bcs
end
function Maxwellian(T, m; v_drift=zeros())
        function P(x,v) 
            if !(v isa Array)
                v = [v]    
            end

            if v_drift == zeros()
                v_drift = zeros(length(v))
            end

            if length(v) != length(v_drift)
                error("v and v_drift should have the same length")
            end

            v_ = sqrt(sum(v .^2))
            v_drift_ = sqrt(sum(v_drift.^2))

            1/(2*π*T) * exp(-(v_ - v_drift_)^2/(2*T))
        end
end

Te = 1	#10_000 # eV
m = 1
q=1

P = Maxwellian(Te, m)

μ_0 = 0		#1.25663706212e-6 # N A⁻²
ϵ_0 = 1	#8.8541878128e-12 # F ms⁻¹ 

lb=0.0;ub=1.0;time_lb=lb;time_ub=ub

dim = 3
@parameters t #vm
xs = Symbolics.variables(:x, 1:dim)
vs = Symbolics.variables(:v, 1:dim)
#v_eq = vm ~ norm(vs)

Dxs = Differential.(xs)
Dt = Differential(t)
#Dvm = Differential(vm)
Dvs = Differential.(vs)

xs_int = xs .∈ Interval(lb, ub) #change lb to -ub
vs_int = vs .∈ Interval(lb, ub)
#vm_int = vm ∈ Interval(lb, ub)
t_int = t ∈ Interval(time_lb, time_ub)
#domains = [t_int;xs_int;vm_int]
domains = [t_int;xs_int;vs_int]

Es = Symbolics.variables(:E, 1:dim; T=SymbolicUtils.FnType{Tuple,Real})
Bs = Symbolics.variables(:B, 1:dim; T=SymbolicUtils.FnType{Tuple,Real})
vm = Symbolics.variable(:vm,T=SymbolicUtils.FnType{Tuple,Real})

_vm = vm(vs...)
_Es = [E(t,xs...,vs...) for E in Es]
_Bs = [B(t,xs...,vs...) for B in Bs]
f=Symbolics.variable(:f;T=SymbolicUtils.FnType{Tuple,Real})
_f=f(t,xs...,_vm)

div_vs = divergence(Dxs, _f, vs)
div_B = divergence(Dxs, _Bs)
div_E = divergence(Dxs, _Es)
F = q/m * (_Es + cross(vs,_Bs))
divv_F = divergence(Dvs, _f, F)

_I = Integral(_vm in DomainSets.ProductDomain(ClosedInterval(0 ,ub)))

# charge and current densities
ρ = q * _I(_f)
J = q * _I(_f) * sum(vs)

# system of equations
vlasov_eq = Dt(_f) ~ - div_vs - divv_F
vm_eq = _vm ~ norm(vs)
curl_E_eqs = curl(_Es, Dxs) .~ Dt.(_Bs)
curl_B_eqs = ϵ_0*μ_0 * Dt.(_Es) .- curl(_Bs, Dxs) .~ - μ_0*J
div_E_eq = div_E ~ ρ/ϵ_0
div_B_eq = div_B ~ 0
eqs = [vlasov_eq; vm_eq; curl_E_eqs; curl_B_eqs; div_E_eq; div_B_eq]

vlasov_ic = f(time_lb,xs...,_vm) ~ P(xs,_vm)
E_bcs=Neumann
#f_bcs=(a=1, g=0)
E_bcs = [E_bcs(t, xs, vs, Dxs, Es, lb); E_bcs(t, xs, vs, Dxs, Es, ub)]
#f_bcs = [Reflective(t, xs, vs, fs, lb, f_bcs.a, f_bcs.g); Reflective(t, xs, vs, fs, ub, f_bcs.a, f_bcs.g)]
bcs = [vlasov_ic; E_bcs]

#vlasov_eq1 = Dt(_f) ~ - div_vm
#f_bc1=(a=1, g=0)
#f_bc1 = [Reflective(t, x1, v1, f1, lb, f_bc1.a, f_bc1.g); Reflective(t, x1, v1, f1, ub, f_bc1.a, f_bc1.g)]
#f_bc1 = f1(t,x1,1)~f1(t,x1,-1)
#bc1=[vlasov_ic1;f_bc1]
#vars_arg1 = [_f1]
vars_arg = [_f; _Es...; _Bs...;_vm]

@named sys = PDESystem(eqs, bcs, domains, [t,xs...,vs...], vars_arg)
#@named sys1 = PDESystem(vlasov_eq, vlasov_ic, domains, [t,xs...,vs...], vars_arg)

strat=NeuralPDE.QuadratureTraining(maxiters=1,batch=1)
#strat=Plasma.NeuralPDE.QuadratureTraining()

il=33
chain=Chain(Dense(7, il, sigmoid_fast), Dense(il, 3il, sigmoid_fast), Dense(3il, il, sigmoid_fast), Dense(il, 7))

discretization = PhysicsInformedNN(chain, strat)
prob = discretize(sys, discretization)

phi = discretization.phi
phi([0, 0, 0, 0, 0], prob.u0)

i=0
stopat=1e-3
cb = function (p, l)
	global i
	i+=1
    println(i,": Current loss is: $l")
    if l<stopat
    	return true
    end
    return false
end

opt=ADAM(0.01)
res = solve(prob, opt, callback = cb, maxiters=10)

prob = remake(prob, u0 = res.minimizer)
res = Optimization.solve(prob, ADAM(0.01); callback = callback, maxiters = 2000)

phi = discretization.phi
phi([0, 0, 0], res.minimizer)
