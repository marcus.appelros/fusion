using LinearAlgebra, ForwardDiff, SpecialFunctions #,Integrals

divergence(F::Function, pt) = sum(diag(ForwardDiff.jacobian(F, pt)))
function curl(J::Matrix)
    Mx, Nx, Px, My, Ny, Py, Mz, Nz, Pz = J
    [Py-Nz, Mz-Px, Nx-My] # ∇×VF
end
curl(F::Function, pt) = curl(ForwardDiff.jacobian(F, pt))
ellipke(m)=ellipk(m),ellipe(m)
#=
function ellipke(m)
	if m==1
		return Inf,1
	end
	f1(t,m)=((1-t^2)*(1-m*t^2))^(-0.5)
	domain=(0,1)
	prob=IntegralProblem(f1,domain,m)
	K=solve(prob,QuadGKJL()).u
	f2(t,m)=(1-t^2)^(-0.5)*(1-m*t^2)^(0.5)
	prob=IntegralProblem(f2,domain,m)
	E=solve(prob,QuadGKJL()).u
	return K,E
end
=#
