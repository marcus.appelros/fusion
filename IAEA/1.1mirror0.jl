include("../system.jl")
include("../utils.jl")

sys=System()
sys.dt=0.01
simSecs=100
n=Int(round(simSecs/sys.dt))
p=Particle(1,0,0,[0,0,0],[0.01,1.0,0])
addParticle!(sys,p)

function mf(loc)
	return [1+loc[1]^2,0,0]
end

sys.magneticField=(t,loc)->mf(loc)
sys.electricField=(t,loc)->[0,0,0]

step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,Int(round(length(sys.locHist)/1000)))

#lines_in_3D(l0,l1,l2)
