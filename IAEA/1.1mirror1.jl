include("../system.jl")
include("../utils.jl")

sys=System()
sys.dt=0.01
simSecs=100
n=Int(round(simSecs/sys.dt))
p=Particle(1,0,0,[1,0,0],[0.01,1.0,0])
addParticle!(sys,p)

function mf(loc)
	return [1+loc[1],1+loc[2],0]
end

mfdir=[1,0]
function mf(loc)
       return [mfdir[1]*loc[1],mfdir[2]*loc[2],0]
end

sys.magneticField=(t,loc)->mf(loc)
sys.electricField=(t,loc)->[0,0,0]

F=forcesParallelMagneticDrift(sys)


#=
rmat=sqrt(2)/2*[1 -1;1 1]
mfdir=rmat*mfdir
mfg=ForwardDiff.gradient(x->norm(mf(x)),rand(3))
mfj=ForwardDiff.jacobian(mf,rand(3))
Bgradpara=diag(mfj)

Fp=[-0.5, -0.0, -0.0] #dBx=1
Ft=[-0.5100500000000001, -0.0, -0.0] #dBx=2

v=p.v[1:2]
rv=rmat*v
p.v[1:2]=rv
F2=forcesParallelMagneticDrift(sys)

p.loc[1:2]=rand(2)
F3=forcesParallelMagneticDrift(sys)

rmat=sqrt(2)/2*[1 -1;1 1]
mfdir=[1,0]
p.v=[0.01,1.0,0]
loc0=[1,0]
p.loc[1:2]=loc0
F4=forcesParallelMagneticDrift(sys)
loc1=rmat*loc0
v=p.v[1:2]
rv=rmat*v
mfdir=rmat*mfdir
mfdir=[1,1]
p.v[1:2]=rv
p.loc[1:2]=loc1
F5=forcesParallelMagneticDrift(sys)
p.loc=[1,1,0]
F6=forcesParallelMagneticDrift(sys)

mfr=r->mf([sqrt(r^2/2),sqrt(r^2/2),0])
ForwardDiff.derivative(mfr,rand())
=#

#mf2xgrad=ForwardDiff.gradient(x->norm(mf(x)),[0,0,0])
#mf1x1ygrad=ForwardDiff.gradient(x->norm(mf(x)),[0,0,0])

#step!(sys,n)

#l0,l1,l2=splitHist(sys.locHist,1,Int(round(length(sys.locHist)/1000)))

#lines_in_3D(l0,l1,l2)
