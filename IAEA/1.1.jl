include("../0.jl")

B=magneticField(0)
b=B/mag(B)
vp=Vector()
for i in eachindex(vs)
	push!(vp,dot(vs[i],b)*b)
end
vo=Vector()
for i in eachindex(vp)
	push!(vo,vs[i]-vp[i])
end
function ddt(v) # == built in diff?
	dv=Vector()
	for i in 1:length(v)-1
		push!(dv,v[i+1]-v[i])
	end
	return dv
end
function centraldiff(v::AbstractVector)			#https://discourse.julialang.org/t/differentiation-without-explicit-function-np-gradient/57784
	dv  = diff(v)/2 # half the derivative
	#a   = [dv[1];dv] # copies first element
	a=deepcopy(dv)
	pushfirst!(a,dv[1])
	b=deepcopy(dv)
	push!(b,dv[end])
	a+=b
	#a .+= [dv;dv[end]] # copies last element, add both results to compute average
	return(a)			#cut off the end elements for better results, much better than ddt
end
function deriv(y::AbstractVector,x::AbstractVector)
	return  centraldiff(y)./centraldiff(x)
end

x=collect(0:sS:sS*nstep)

mvo=mag.(vo)
dcvosq=ddt(mvo.^2)
cdcvosq=centraldiff(mvo.^2)

dcvosq1=ddt(dot.(vo,vo))
cdcvosq1=centraldiff(dot.(vo,vo))

#dvo=ddt(vo)
dvo=deriv(vo,x)
cdvo=centraldiff(vo)

crhs0=2*dot.(vo,cdvo)
rhs0=2*dot.(vo,dvo)
er=mag(rhs0[1:end-1])		#sS 0.1 0.3135616178667456 0.01: 0.03162024698856508 
cer=mag(crhs0[2:end-1])		#sS 0.1 4.611873676289842e-15, 0.01: 4.5474383085604397e-14

#using Plots
#plot(crhs0[2:end-1])

#using Equations
q=1
m=1
v=[1,0,0]
vpa=dot(v,b)*b
voa=v-vpa
dv=q/m*cross(v,B)
dvp=dot(dv,b)*b
dvoa=dv-dvp
vps=dot(v,b)*b
vos=v-vps
dot(vos,dvoa)

ddv=q/m*cross(dv,B)
ddvp=dot(ddv,b)*b
ddvoa=ddv-ddvp

ddvoa # ==
-(q*norm(B)/m)^2*voa

#using Interpolations
#vitp=linear_interpolation(collect(1:length(vo)),vo)
#2*dot(vo[500],gradient(vitp,500)[1])
#dva=ddt(vs)
#cdv=centraldiff(vs)
#vitpc=cubic_spline_interpolation((1:100,),vs)


#a=rand(3);b=rand(3);c=rand(3)
#t0=cross(cross(a,b),c)
#t1=dot(a,c)*b-dot(b,c)*a

ddv0=centraldiff(cdvo)
ddvo=deriv(dvo,x)
t2=-(q*norm(B)/m)^2*vo

#plot(mag.(ddvo[3:end-3]))
#plot!(mag.(t2[3:end-3]))
