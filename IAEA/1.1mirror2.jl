include("../system.jl")
include("../utils.jl")

sys=System()
sys.dt=0.01
simSecs=30
n=Int(round(simSecs/sys.dt))
p=Particle(1,0,0,[0.0,0,0],[1,1.0,0])
addParticle!(sys,p)

function mf(loc)
	r=loc[2:3]
	#Bx=x->1.0+abs(x)
	Bx=x->1+x^2
	dBx=ForwardDiff.derivative(Bx,loc[1])
	Br=-r/2*dBx
	return [Bx(loc[1]),Br[1],Br[2]]
end

divergence(F::Function, pt) = sum(diag(ForwardDiff.jacobian(F, pt)))

sys.magneticField=(t,loc)->mf(loc)
sys.electricField=(t,loc)->[0,0,0]


s=sys
B0=s.magneticField(s.t,p.loc)
b=B0/norm(B0)
m=mass(p)
v0=p.v
vpara=dot(v0,b)*b
vorth=v0-vpara
μ=m*norm(vorth)^2/(2*norm(B0))			#magnetic moment
sina=norm(vorth)/norm(v0)
Bbounce=norm(B0)/sina^2
En=0.5*m*norm(v0)^2
Bgrad0=ForwardDiff.jacobian(loc->s.magneticField(s.t,loc),p.loc)
Bgradpara0=diag(Bgrad0)
F0=-μ*Bgradpara0

#=
step!(s,100);
v1=p.v
En1=0.5*m*norm(v1)^2
vpara1=dot(v1,b)*b
vorth1=v1-vpara1
B1=s.magneticField(s.t,p.loc)
μ1=m*norm(vorth1)^2/(2*norm(B1))			#magnetic moment
Bgrad1=ForwardDiff.jacobian(loc->s.magneticField(s.t,loc),p.loc)
Bgradpara1=diag(Bgrad1)
F1=-μ1*Bgradpara1
Fs=forcesParallelMagneticDrift(s)

=#
#=
#d.dt=0.001
vend=s.vHist[20297][1]
Enend=0.5*m*norm(vend)^2
Uend=En-Enend #potential energy
vendn=s.vHist[20397][1]
Enendn=0.5*m*norm(vendn)^2
Uendn=En-Enendn
dU=(Uendn-Uend)/(s.dt*100)

lend=s.locHist[20297][1]
Bend=s.magneticField(0,lend)
μend=m*norm(vorth)^2/(2*norm(Bend))			#magnetic moment
Bgrad=ForwardDiff.jacobian(loc->s.magneticField(s.t,loc),lend)
Bgradpara=diag(Bgrad)
Fend=-μend*Bgradpara
=#

#F=forcesParallelMagneticDrift(sys)

step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,Int(round(length(sys.locHist)/1000)))

#lines_in_3D(l0,l1,l2)



