include("../system.jl")
include("../utils.jl")

sys=System()
p=Particle(1,0,0,[0,0,0],[10.0,0.0,0])
#push!(sys.particles,p)
addParticle!(sys,p)
sys.magneticField=(t,loc)->[0,1,0]
sys.electricField=(t,loc)->[1+t/10,0,0]
n=10000
step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,Int(n/1000))

#lines_in_3D(l0,l1,l2)

q=charge(p)
Bmag=norm(sys.magneticField(0,[0,0,0]))
m=mass(p)
ωc=q*Bmag/m #cyclotron frequency
dE=0.1
vpol=1/(ωc*Bmag)*dE

#=
s1=System()
push!(s1.particles,p)
s1.step=0
s2=System()
addParticle!(s2,p)
s1.magneticField=(t,loc)->[0,1,0]
s2.electricField=(t,loc)->[1+t/10,0,0]
s2.magneticField=(t,loc)->[0,1,0]
s1.electricField=(t,loc)->[1+t/10,0,0]
=#
