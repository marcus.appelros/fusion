include("../system.jl")
include("../utils.jl")
using ForwardDiff

sys=System()
simSecs=400/200
n=10000
sys.dt=simSecs/n
p=Particle(0,0,1,[2,0,0],[0.0,1.0,0])
addParticle!(sys,p)

circle(angle,r=1,z=0) = [r*cos(angle), r*sin(angle), z]
function mf(loc,decExp=0)
	r=norm(loc)
	angle=acos(loc[1]/r)
	if loc[2]<0
		angle=2pi-angle
	end
	f(α)=circle(α,r,loc[3])
	B=ForwardDiff.derivative(f,angle)
	return B/norm(B)*r^-decExp
end

#dec=0
sys.magneticField=(t,loc)->mf(loc)
sys.electricField=(t,loc)->[0,0,0]

step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,Int(round(length(sys.locHist)/1000)))

#dec>0
reset!(sys)
sys.magneticField=(t,loc)->10*mf(loc,2)
step!(sys,n);



#l0,l1,l2=splitHist(sys.locHist,1,Int(n/1000))

#=
lines_in_3D(l0,l1,l2)

ts=range(0, 2pi, length=100)

fig=xyz()
plotFun!(circle,ts)
cs=circle.(ts[1:10:end])
arrows!(Point3.(cs),Point3.(mf.(cs)))

traj(t)=[2,t,0]
traja=traj.(ts[1:60])
lines!(Point3.(traja))

lines!(l0[1:40000],l1[1:40000],l2[1:40000])

=#
