include("../system.jl")
include("../utils.jl")

sys=System()
simSecs=100
n=10000
sys.dt=simSecs/n
p=Particle(1,0,0,[0,0,0],[0.0,0.0,0])
addParticle!(sys,p)
By=10
sys.magneticField=(t,loc)->[0,By,0]
Ex=1
sys.electricField=(t,loc)->[sin(t/(simSecs/10))<0 ? -Ex : Ex,0,0]

step!(sys,n)

l0,l1,l2=splitHist(sys.locHist,1,Int(n/1000))

#lines_in_3D(l0,l1,l2)

#=
Ex=1
reset!(sys)
sys.electricField=(t,loc)->[Ex,0,0]
step!(sys,n)
l0,l1,l2=splitHist(sys.locHist,1,Int(n/1000))
lines_in_3D(l0,l1,l2)

sys.electricField=(t,loc)->[Ex,0,0]

reset!(sys)
sys.particles[1].v=[0.0,0,0]
By=3.
step!(sys,n)
l0,l1,l2=splitHist(sys.locHist,1,Int(n/1000))
lines_in_3D(l0,l1,l2)
=#
