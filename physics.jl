include("math.jl")

#1 eV = 1.602×10−19 J
#e = 1.602176634×10−19 C
function charge(p::Particle)
	return p.protons-p.electrons
end
#proton mass=1.67262189821×10^−24 g -> 1
#neutron mass=1.67492749804×10^−24 g -> 1.0013784345598173
#electron mass=9.1093837015×10^−28 g -> 0.0005446170297811266
function mass(p::Particle)			#TODO benchmark ::FLoat64
	return p.protons+p.neutrons*1.0013784345598173+p.electrons*0.0005446170297811266			#TODO mass defect, better to have individual H,D,T,He?
end

#F=k*Q1*Q2/d^2			https://www.physicsclassroom.com/class/estatics/Lesson-3/Coulomb-s-Law	
#a=F/m
function distance(p0::Particle,p1::Particle)
	d=0.0
	@simd for i in eachindex(p0.loc)			#TODO Benchmark @simd difference, compare to norm
		d+=abs(p0.loc[i] - p1.loc[i])
	end
	return d
end
function diruv(v0::Vector,v1::Vector) #directional unit vector
	v=v1-v0
	return v/mag(v)
end
function methodBoris(s::System)
	vv=Vector()
	allstuck=true
	for pind in eachindex(s.particles)
		if s.stuck[pind]>0
			push!(vv,[0.0,0,0])
			continue
		end
		allstuck=false
		p=s.particles[pind]
		for c in sys.components
			if shield(c,s.t,p.loc)
				s.stuck[pind]=s.step
			end
		end
		m=mass(p)
		q=charge(p)
		vAc=1
		B=s.magneticField(s.t,p.loc)
		for c in sys.components
			B+=magneticField(c,s.t,p.loc)
		end
		E=s.electricField(s.t,p.loc)
		t = q / m * B * 0.5 * s.dt;
		u = 2. * t ./ (1. .+ t.*t); #renamed from s
		v_minus = p.v + q / (m * vAc) * E * 0.5 * s.dt;
		v_prime = v_minus + cross(v_minus,t);
		v_plus = v_minus + cross(v_prime,u);
		v = v_plus + q / (m * vAc) * E * 0.5 * s.dt;
		push!(vv,v)
	end
	s.allStuck=allstuck
	return vv
end
function forcesParallelMagneticDrift(s::System)
	forces=Vector()
	for i in eachindex(s.particles)
		p=s.particles[i]
		m=mass(p)
		B=s.magneticField(s.t,p.loc)
		b=B/norm(B)
		vpara=dot(p.v,b)*b
		vorth=p.v-vpara
		μ=m*norm(vorth)^2/(2*norm(B))			#magnetic moment
		Bgrad=ForwardDiff.jacobian(loc->s.magneticField(s.t,loc),p.loc)
		Bgradpara=diag(Bgrad)
		Fpara=-μ*Bgradpara
		push!(forces,Fpara)
	end
	return forces
end
function forceCoulomb(p0::Particle,p1::Particle) #force on p0			TODO optimmize for allocations, preallocate?
	q0=charge(p0)
	q1=charge(p1)
	d=distance(p0,p1)
	F=q0*q1/d^2 #k=1, Coulomb's law constant
	Fv=-diruv(p0.loc,p1.loc)*F
	return Fv
end
function forcesCoulomb(s::System)
	np=length(s.particles)
	forces=Vector{Vector{Float64}}(undef,np)
	#fill!(forces,zeros(length(s.particles[1].v)))			#TODO benchmark hardcoding this to 3. LINK 0
	for i in 1:np
		forces[i]=zeros(length(s.particles[1].v))
	end
	for i in 1:np
		for j in 1:np
			if i==j
				continue
			end
			Fv=forceCoulomb(s.particles[i],s.particles[j])
			forces[i]+=Fv
			forces[j]-=Fv
		end
	end	
	return forces
end
function f2a(s::System,forces::Vector)			#TODx benchmark baking this into forcesCoulomb. No we need it for other forces
	np=length(s.particles)
	a=Vector()
	for i in 1:np
		push!(a,forces[i]/mass(s.particles[i]))
	end
	return a
end
function forcesLorentz(s::System)
	np=length(s.particles)
	forces=Vector()
	for i in 1:np
		p=s.particles[i]			#TODx benchmark difference between assigning and looking up. Looking up seems marginally faster
		q=charge(p)
		B=s.magneticField(p.loc)
		push!(forces,q*cross(p.v,B))
	end
	return forces
end
function applyL!(s::System,i::Int,aL)
	m=norm(s.particles[i].v)
	s.particles[i].v+=aL*s.stepSize
	x=norm(s.particles[i].v)/m
	s.particles[i].v/=x
end
function forcesElectric(s::System)
	np=length(s.particles)
	forces=Vector()
	for i in 1:np
		p=s.particles[i]
		q=charge(p)
		E=s.electricField(p.loc)
		push!(forces,q*E)
	end
	return forces
end
