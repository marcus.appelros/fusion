mutable struct Particle
	protons::Int
	neutrons::Int
	electrons::Int
	loc::Vector{Float64}
	v::Vector{Float64}
end
mutable struct System
	particles::Vector{Particle}
	dt::Float64
	magneticField
	electricField
	t::Float64
	step::Int
	tHist::Vector{Float64}
	locHist::Vector
	vHist::Vector
	aHist::Vector
end
System()=System([],0.01,0,0,0,0,[],[],[],[])
System(a)=System(a,0.01,0,0,0,0,[],[],[],[])
System(a,sS)=System(a,sS,0,0,0,0,[],[],[],[])

include("physics.jl")
using Interpolations

function methodMidpoint(a1,a0,sS)
	aitp=linear_interpolation(collect(1:2),[a0,a1],extrapolation_bc = Line())
	return sS*aitp(2.5)
end
function aMidpoint(s::System)
	sc=deepcopy(s)
	sc.dt/=2
	step!(sc,true,false)
	step!(sc,true,false)
	return sc.aHist[end]
end
function methodRK0(s::System) #doesn't work first step
	sS=s.dt
	sc=System()
	sc.particles=deepcopy(s.particles)
	sc.magneticField=s.magneticField
	sc.electricField=s.electricField
	k1=sS*f2a(s,forcesLorentz(s))
	for i in eachindex(sc.particles)
		sc.particles[i].loc+=sS*s.vHist[end][i]/2
	end
	pc=deepcopy(sc.particles)
	for i in eachindex(sc.particles)
		sc.particles[i].v+=k1/2
	end
	k2=sS*f2a(sc,forcesLorentz(sc))
	sc.particles=pc
	for i in eachindex(sc.particles)
		sc.particles[i].v+=k2/2
	end
	k3=sS*f2a(sc,forcesLorentz(sc))
	sc.particles=deepcopy(s.particles)
	for i in eachindex(sc.particles)
		sc.particles[i].loc+=sS*s.vHist[end][i]
	end
	for i in eachindex(sc.particles)
		sc.particles[i].v+=k3
	end
	k4=sS*f2a(sc,forcesLorentz(sc))
	return k1/6+k2/3+k3/3+k4/6
end
function methodRK(s::System) #TODO apply B scaling, add other forces
	sS=s.dt
	sc=System()
	sc.particles=deepcopy(s.particles)
	sc.magneticField=s.magneticField
	sc.electricField=s.electricField
	k1=f2a(sc,forcesLorentz(sc))
	for i in eachindex(sc.particles)
		#sc.particles[i].v+=k1[i]/2
		applyL!(sc,i,k1[i]/2)
	end
	for i in eachindex(sc.particles)
		sc.particles[i].loc+=sS*sc.particles[i].v/2
	end
	k2=f2a(sc,forcesLorentz(sc))
	sc.particles=deepcopy(s.particles)
	for i in eachindex(sc.particles)
		#sc.particles[i].v+=k2[i]/2
		applyL!(sc,i,k2[i]/2)
	end
	for i in eachindex(sc.particles)
		sc.particles[i].loc+=sS*sc.particles[i].v/2
	end
	k3=f2a(sc,forcesLorentz(sc))
	sc.particles=deepcopy(s.particles)
	for i in eachindex(sc.particles)
		#sc.particles[i].v+=k3[i]
		applyL!(sc,i,k3[i])
	end
	for i in eachindex(sc.particles)
		sc.particles[i].loc+=sS*sc.particles[i].v
	end
	k4=f2a(sc,forcesLorentz(sc))
	return (k1/6+k2/3+k3/3+k4/6)
end


function step!(s::System,useEuler=true,useRK=false)
	useMida=false
	s.step+=1
	push!(s.locHist,[])
	push!(s.vHist,[])
	push!(s.aHist,[])
	if useMida
		anext=aMidpoint(s)
	end
	#= #this is just wonky
	a=f2a(s,forcesLorentz(s))
	for i in eachindex(a)
		m=norm(s.particles[i].v)
		#s.particles[i].v+=a[i]*s.dt
		if useEuler || s.step==1 #use Euler method
			s.particles[i].v+=a[i]*s.dt
		else
			s.particles[i].v+=methodMidpoint(a[i],s.aHist[s.step-1][i][1:3],s.dt)
		end
		x=norm(s.particles[i].v)/m
		s.particles[i].v/=x
		push!(s.aHist[s.step],a[i])
		push!(s.aHist[s.step][i],a[i][1],a[i][2],a[i][3])
	end
	a=f2a(s,forcesCoulomb(s))
	a+=f2a(s,forcesElectric(s))
	for i in eachindex(a)
		if useEuler || s.step==1 #use Euler method
			s.particles[i].v+=a[i]*s.dt
		else
			s.particles[i].v+=methodMidpoint(a[i],s.aHist[s.step-1][i][4:6],s.dt)
		end
		push!(s.aHist[s.step][i],a[i][1],a[i][2],a[i][3])
	end
	=#
	#==#
	a=f2a(s,forcesCoulomb(s))
	a+=f2a(s,forcesElectric(s))
	if useRK
		aL=methodRK(s)
	else
		aL=f2a(s,forcesLorentz(s))
	end
	for i in eachindex(a)
		if useMida 
			s.particles[i].v+=anext[i][1:3]*s.dt
		elseif useEuler || s.step==1 #use Euler method
			s.particles[i].v+=a[i]*s.dt
		else
			s.particles[i].v+=methodMidpoint(a[i],s.aHist[s.step-1][i][1:3],s.dt)
		end
		push!(s.aHist[s.step],a[i])
	end
	for i in eachindex(a)
		#=m=norm(s.particles[i].v)
		#s.particles[i].v+=a[i]*s.dt
		if useMida 
			s.particles[i].v+=anext[i][4:6]*s.dt
		elseif useEuler || s.step==1 #use Euler method
			s.particles[i].v+=aL[i]*s.dt
		else
			s.particles[i].v+=methodMidpoint(aL[i],s.aHist[s.step-1][i][4:6],s.dt)
		end
		x=norm(s.particles[i].v)/m
		s.particles[i].v/=x =#
		applyL!(s,i,aL[i])
		push!(s.aHist[s.step][i],aL[i][1],aL[i][2],aL[i][3])
	end
	#==#
	for p in s.particles
		d=p.v*s.dt
		p.loc+=d
		push!(s.vHist[s.step],p.v)
		push!(s.locHist[s.step],p.loc)
	end
	s.t+=s.dt
	push!(s.tHist,s.t)
	return s
end
