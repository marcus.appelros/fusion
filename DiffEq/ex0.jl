using DifferentialEquations, ModelingToolkit
using ModelingToolkit: t_nounits as t_, D_nounits as D_

@parameters σ ρ β
@variables x(t_) y(t_) z(t_)

eqs = [D_(D_(x)) ~ σ * (y - x),
    D_(y) ~ x * (ρ - z) - y,
    D_(z) ~ x * y - β * z]

@mtkbuild sys = ODESystem(eqs, t_)

u0 = [D_(x) => 2.0,
    x => 1.0,
    y => 0.0,
    z => 0.0]

p = [σ => 28.0,
    ρ => 10.0,
    β => 8 / 3]

tspan = (0.0, 100.0)
prob = ODEProblem(sys, u0, tspan, p, jac = true)
sol = solve(prob)
#using Plots
#plot(sol, idxs = (x, y))

#@named sys2 = ODESystem(eqs, t_,[x,y,z],[σ,ρ,β],tspan=tspan)
#prob2 = ODEProblem(sys2, u0, tspan, p, jac = true)
#sol2 = solve(prob2)

