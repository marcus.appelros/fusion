using GLMakie
GLMakie.activate!()
function lines_in_3D(x,y,z)
	n=length(x)
	aspect=(1, 1, 1)
	perspectiveness=0.5
	# the figure
	fig = Figure(; size=(1200, 500))
	ax1 = Axis3(fig[1, 1]; aspect, perspectiveness)
	#ax2 = Axis3(fig[1, 2]; aspect, perspectiveness)
	ax3 = Axis3(fig[1, 2]; aspect=:data, perspectiveness)
	lines!(ax1, x, y, z; color=1:n, linewidth=3)
	#scatterlines!(ax2, x, y, z; markersize=15)
	hm = meshscatter!(ax3, x, y, z; markersize=0.2, color=1:n)
	#hm = meshscatter!(ax3, x, y, z; markersize=0.2, color=sin.(collect(1:n)./10))
	lines!(ax3, x, y, z; color=1:n)
	Colorbar(fig[2, 1], hm; label="values", height=15, vertical=false,
		flipaxis=false, ticksize=15, tickalign=1, width=Relative(3.55/4))
	fig
end
function xyz()
	orig=Vector{Point3}()
	push!(orig,Point3(0.0,0,0),Point3(0.0,0,0),Point3(0.0,0,0))
	xyz=Vector{Point3}()
	push!(xyz,Point3(1.0,0,0),Point3(0.0,1,0),Point3(0.0,0,1))
	fig=arrows(orig,xyz,linewidth=0.05,arrowsize=0.3)
	fs=30
	text!("x", position=xyz[1], fontsize=fs)
	text!("y", position=xyz[2], fontsize=fs)
	text!("z", position=xyz[3], fontsize=fs)
	return fig
end
function plotFun!(f::Function,ts,arro=false)
	lines!(Point3.(f.(ts)))
end
#=
function plotField!(f::Function,bb=5,step=1)
	bases=Vector()
	heads=Vector()
	for x in -bb:step:bb
		for y in -bb:step:bb
			for z in -bb:step:bb
				push!(bases,[x,y,z])
				push!(heads,f([x,y,z]))
			end
		end
	end
	fmax=maximum(norm.(heads))
	heads./=(2*fmax/step)
	arrows!(Point3.(bases),Point3.(heads))
end
=#
function plotField!(f::Function,z=0,bb=5,step=1)
	bases=Vector()
	heads=Vector()
	for x in -bb:step:bb
		for y in -bb:step:bb
			push!(bases,[x,y,z])
			fl=f([x,y,z])
			#println(fl)
			push!(heads,fl)
		end
	end
	fmax=maximum(norm.(heads))
	if isnan(fmax);error("Calculation produces NaN.");end
	heads./=(fmax/step)
	arrows!(Point3.(bases),Point3.(heads))
end

#TODO plot Bosch tokamak

function splitHist(l::Vector,i::Int=1,skip::Int=1)
	l0=Vector{Float64}()
	l1=Vector{Float64}()
	l2=Vector{Float64}()
	for li in 1:skip:length(l)
		la=l[li]
		push!(l0,la[i][1])
		push!(l1,la[i][2])
		push!(l2,la[i][3])
	end
	return l0,l1,l2
end
