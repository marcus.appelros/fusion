#ref: https://github.com/iwhoppock/boris-algorithm/blob/master/boris.py

using LinearAlgebra
include("math.jl")
include("utils.jl")
include("Bosch/B1.jl")

function B_norm(loc)
	I1 = 1_000_000; #current in the solenoid
	I2 = 0; #current in the central solenoid
	
	dist = 10; #distance between solenoids
	a = 1; #radius of each coil
	b = 4; #radius of central coil
	B0 = 0.009478723182526037 #norm(B1(0, 0, 0, dist, a, b, I1, I2))
	return B1(loc..., dist, a, b, I1, I2)/B0
end
@assert norm(B_norm([0,0,0]))==1.0

function f(n::Int,dt,vAc=1)
#	dt = 1e-2;
	mass = 1.;
	charge = 1.;
#	vAc = 3e-4;

	duration = n;
	vp=0.0001
	v = [vp, 0., vp];
	x = [0., 0., 0.];

	#B = [0., 1., 0.];
	B = B_norm([0,0,0])
	E = [0., 0., 0.];

	X = zeros((duration,3)) 
	V = zeros((duration,3)) 

	for time in 1:duration
	    t = charge / mass * B * 0.5 * dt;
	    s = 2. * t ./ (1. .+ t.*t);
	    v_minus = v + charge / (mass * vAc) * E * 0.5 * dt;
	    v_prime = v_minus + cross(v_minus,t);
	    v_plus = v_minus + cross(v_prime,s);
	    v = v_plus + charge / (mass * vAc) * E * 0.5 * dt;
	    x += v * dt;	#v is exact in uniform E but x is off by 1/(2n) if dt=1/n -> dt/2
	    X[time,:] = x;
	    V[time,:] = v; 
	    B = B_norm(x)
	end
	return X,v
end

n=3000
X,v=f(n,0.01)
#lines_in_3D(X[:,1],X[:,2],X[:,3])
zmax,zi=findmax(X[:,3])
B_bounce=B_norm(X[zi,:]) #~45

println("The particle bounces at $zmax where the magnetic field strength is $(norm(B_bounce)).") # -> "The particle bounces at 4.459710354302382 where the magnetic field strength is 47.82221169832761."

#using Roots
#vAc=find_zero(vAc->f(n,1/n,vAc)[2][1],1.005) #1

