using GLMakie
GLMakie.activate!()

points=Vector()
push!(points,[0.0,0,0],[1.0,0,0],[0.0,1,0],[0.0,0,1])
dirs=Vector()
push!(dirs,[0.0,0,0],[1.0,0,0],[0.0,1,0],[0.0,0,1])

arrows(points,dirs)

points=Vector{Point3}()
push!(points,Point3(0.0,0,0),Point3(1.0,0,0),Point3(0.0,1,0),Point3(0.0,0,1))
dirs=Vector{Point3}()
push!(dirs,Point3(2.0,2,5),Point3(1.0,0,0),Point3(0.0,1,0),Point3(0.0,0,1))

arrows(points,dirs)

using ForwardDiff
r(t) = [sin(t), cos(t), t] # vector, not tuple
ts = range(0, 4pi, length=200)
#scene = Scene()
#lines!(scene, Point3.(r.(ts)))

nts = pi:pi/4:3pi
us = r.(nts)
dus = ForwardDiff.derivative.(r, nts)

#arrows!(scene, Point3.(us), Point3.(dus))

lines(Point3.(r.(ts)))
arrows!(Point3.(us), Point3.(dus))


#centrifugal force
using LinearAlgebra

#circle(angle,r,z=0) = [r*sin(angle), r*cos(angle), z]
circle(angle,r,z=0) = [r*cos(angle), r*sin(angle), z]
ts = range(0, 2pi, length=100)
nts = range(0, 2pi, length=10)
us1 = circle.(nts,1)
us3 = circle.(nts,3)
#dus = ForwardDiff.derivative.(r, nts)

orig=Vector{Point3}()
push!(orig,Point3(0.0,0,0),Point3(0.0,0,0),Point3(0.0,0,0))
xyz=Vector{Point3}()
push!(xyz,Point3(1.0,0,0),Point3(0.0,1,0),Point3(0.0,0,1))

#arrows(orig,xyz)
#arrows(orig,xyz,linewidth=0.01)
arrows(orig,xyz,linewidth=0.05,arrowsize=0.3)
fs=30
text!("x", position=xyz[1], fontsize=fs)
text!("y", position=xyz[2], fontsize=fs)
text!("z", position=xyz[3], fontsize=fs)

#lines(Point3.([[0,0,0.0],[1,1,1.0]]))
lines!(Point3.(circle.(ts,1)))
lines!(Point3.(circle.(ts,2)))
lines!(Point3.(circle.(ts,3)))
lines!(Point3.(circle.(ts,2,1)))
lines!(Point3.(circle.(ts,2,-1)))
dus1 = ForwardDiff.derivative.(t->circle(t,1), nts)
dus3 = ForwardDiff.derivative.(t->circle(t,3), nts)
dus3./=norm.(dus3)
arrows!(Point3.(us1), Point3.(dus1))
arrows!(Point3.(us3), Point3.(dus3))

traj(t)=[2,t,0]
traja=traj.(ts)
trajan=traj.(nts)
scatter!(Point3.(traja))

function mf(loc,decExp=0)
	r=norm(loc)
	angle=acos(loc[1]/r)
	if loc[2]<0
		angle=2pi-angle
	end
	f(α)=circle(α,r,loc[3])
	B=ForwardDiff.derivative(f,angle)
	return B/norm(B)*r^-decExp
end

trajmf=mf.(trajan)
arrows!(Point3.(trajan),Point3.(trajmf))



#surface
peaks(x,y) = 3*(1-x)^2*exp(-x^2 - (y+1)^2) - 10(x/5-x^3-y^5)*exp(-x^2-y^2)- 1/3*exp(-(x+1)^2-y^2)
xs = ys = range(-5, 5, length=25)
surface(xs, ys, peaks)
