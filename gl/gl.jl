using GLFW, ModernGL

function launch()
	window = GLFW.CreateWindow(640, 480, "GLFW.jl")
	GLFW.MakeContextCurrent(window)
	
	#ctx = ModernGL.create_context()
	
	#vertices = [(0, 0.5), (0.5, -0.5), (-0.5, -0.5)]
	#glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW)
	
	# Shaders & Program
#=
	prog = ctx.program([
	    ctx.vertex_shader("""
		#version 330

		in vec2 vert;

		void main() {
		    gl_Position = vec4(vert, 0.0, 1.0);
		}
	    """),
	    ctx.fragment_shader("""
		#version 330

		out vec4 color;

		void main() {
		    color = vec4(0.3, 0.5, 1.0, 1.0);
		}
	    """),
	])
	prog = glCreateProgram()
	glAttachShader(prog, vertexShader)

	# Buffer
	
	#struct.pack('6f',0.0, 0.8,-0.6, -0.8,0.6, -0.8,)
	vertices=[0.0,0.8,-0.6,-0.8,0.6,-0.8]

	vbo = ctx.buffer(vertices)

	# Put everything together

	vao = ctx.simple_vertex_array(prog, vbo, ["vert"])

=#	
	while !GLFW.WindowShouldClose(window)
		glClear(GL_COLOR_BUFFER_BIT)
		# Render here
		#glDrawArrays(GL_TRIANGLES, 0, length(vertices))
		#vao.render()

		# Swap front and back buffers
		GLFW.SwapBuffers(window)

		# Poll for and process events
		GLFW.PollEvents()
		yield() #@async launch
	end
	GLFW.DestroyWindow(window)
end
