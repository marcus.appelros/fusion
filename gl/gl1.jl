import GLFW
using ModernGL
include("gltestutil.jl")

GLFW.Init()
# OS X-specific GLFW hints to initialize the correct version of OpenGL
wh = 600
# Create a windowed mode window and its OpenGL context
window = GLFW.CreateWindow(wh, wh, "OpenGL Example")
# Make the window's context current
GLFW.MakeContextCurrent(window)
GLFW.ShowWindow(window)
GLFW.SetWindowSize(window, wh, wh) # Seems to be necessary to guarantee that window > 0

glViewport(0, 0, wh, wh)

createcontextinfo()

data0 = GLfloat[
	0.0, 0.0,
	0.5, 0.0,
	0.0,0.5
]
data1 = GLfloat[
	0.30, 0.30, 0.0,
	0.5, 0.35, 0.0, #how can a triangle with two identical vertices be visible?
	0.5,0.35, 0.0
]
data = GLfloat[
	0.0, 0.0, 0.0,
	0.5, 0.0, 0.0,
	0.0,0.5, 0.0
]
# Generate a vertex array and array buffer for our data
vao = glGenVertexArray()
glBindVertexArray(vao)
vbo = glGenBuffer()
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW)
# Create and initialize shaders
const vsh0 = """
$(get_glsl_version_string())
in vec2 position;
void main() {
gl_Position = vec4(position, 0.0, 1.0);
}
"""
const vsh1 = """
$(get_glsl_version_string())
//#extension GL_EXT_debug_printf : enable

in vec3 position;

uniform mat4 trans;

void main() {
//debugPrintfEXT("p2 is %f", position[2]);
//debugPrintfEXT(position[0]);
//printf("p2 is %f", position[2]);
//printf(position[0]);
if (position[2]==0.0) {
gl_Position = trans*vec4(position, 5.0);
} else {
gl_Position = trans*vec4(position, 1);
}
}
"""
const vsh = """
$(get_glsl_version_string())

in vec3 position;

out vec4 Color;

uniform mat4 trans;

void main() {
gl_Position = trans*vec4(position, 1);
if (position[0]==0.0) {
//gl_Position = trans*vec4(position, 1.0);
	Color=vec4(1.0,0.0,0.0,1.0);
} else {
//gl_Position = trans*vec4(position, 1);
Color=vec4(0.0,1.0,0.0,1.0);
}
}
"""
const fsh0 = """
$(get_glsl_version_string())
out vec4 outColor;
void main() {
outColor = vec4(1.0, 0.0, 1.0, 1.0);
}
"""
const fsh1 = """
$(get_glsl_version_string())
uniform vec3 triangleColor;
out vec4 outColor;
void main() {
outColor = vec4(triangleColor, 1.0);
}
"""
const fsh = """
$(get_glsl_version_string())
in vec4 Color;
out vec4 outColor;
void main() {
outColor = Color; //vec4(triangleColor, 1.0);
}
"""
vertexShader = createShader(vsh, GL_VERTEX_SHADER)
fragmentShader = createShader(fsh, GL_FRAGMENT_SHADER)
program = createShaderProgram(vertexShader, fragmentShader)

uniColor = glGetUniformLocation(program, "triangleColor") #added
trans = glGetUniformLocation(program, "trans")
#glUniform3f(uniColor, 1.0f0, 0.0f0, 0.0f0)

glUseProgram(program)
positionAttribute = glGetAttribLocation(program, "position");
glEnableVertexAttribArray(positionAttribute)
glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, false, 0, C_NULL) #changed from 2 to 3

function rotmat_z(angle::T) where T
    T0, T1 = zero(T), one(T)
    [
        cos(angle), sin(angle), T0, T0,
        -sin(angle), cos(angle),  T0, T0,
        T0, T0, T1, T0,
        T0, T0, T0, T1
    ]
end

GLFW.SetKeyCallback(window, (_, button, _, action, _) -> begin
	if button==GLFW.KEY_S && action == GLFW.PRESS
		println("Step.")
	end
end)

while !GLFW.WindowShouldClose(window)
	glClear(GL_COLOR_BUFFER_BIT)
	# Render here
	#glDrawArrays(GL_TRIANGLES, 0, length(vertices))
	#vao.render()
	glDrawArrays(GL_TRIANGLES, 0, 9) #3 or 6?
	#glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0) #doesnt work

	#glDrawArrays(GL_LINES, 0, 6)
	#glDrawArrays(GL_POINTS, 0, 6)
	glUniform3f(uniColor, 1.0, abs(sin(time())), 0.0)
	glUniformMatrix4fv(trans,1,GL_FALSE,rotmat_z(Float32(time()%100)))
	#glUniformMatrix4fv(trans,1,GL_FALSE,[1.0f0, 0, 0, 0,0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]) #works

	# Swap front and back buffers
	GLFW.SwapBuffers(window)

	# Poll for and process events
	GLFW.PollEvents()
	if GLFW.GetKey(window, GLFW.KEY_S) == GLFW.PRESS
		println("step")
	end
	if GLFW.GetKey(window, GLFW.KEY_ESCAPE) == GLFW.PRESS
		GLFW.SetWindowShouldClose(window, true)
	end
end
GLFW.DestroyWindow(window)
