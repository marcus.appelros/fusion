import GLFW
using ModernGL, LinearAlgebra

include("glutil.jl")

eye4=Float32[1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1]

window = GLFW.CreateWindow(640, 480, "Hello Triangle")
GLFW.MakeContextCurrent(window)

createcontextinfo()

data = GLfloat[
	0.5, 0.0, -1/(2*sqrt(2)),
	-0.5, 0.0, -1/(2*sqrt(2)),
	0.0,0.5, 1/(2*sqrt(2)),
	0.5, 0.0, -1/(2*sqrt(2)),
	-0.5, 0.0, -1/(2*sqrt(2)),
	0,-0.5,1/(2*sqrt(2)),
	
]

vao = glGenVertexArray()
glBindVertexArray(vao)
vbo = glGenBuffer()
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW)

#glEnable(GL_DEPTH_TEST)			#Could it be easy as adding this for depth testing?
#glDepthFunc(GL_LESS)

const vsh = """
$(get_glsl_version_string())
in vec3 position;
uniform mat4 view;
uniform mat4 move;
uniform mat4 proj;
void main() {
gl_Position = move*proj*view*vec4(position, 1.0);
}
"""
const fsh = """
$(get_glsl_version_string())
out vec4 outColor;
void main() {
outColor = vec4(0.0, 1.0, 0.0, 1.0);
}
"""

vertexShader = createShader(vsh, GL_VERTEX_SHADER)
fragmentShader = createShader(fsh, GL_FRAGMENT_SHADER)

program = glCreateProgram()
glAttachShader(program, vertexShader)
glAttachShader(program, fragmentShader)
glLinkProgram(program)

glUseProgram(program)
positionAttribute = glGetAttribLocation(program, "position");
glEnableVertexAttribArray(positionAttribute)
glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)

function lookat(eyePos, lookAt, up)
	z  = normalize(eyePos-lookAt)
	x  = normalize(cross(up, z))
	y  = normalize(cross(z, x))
	T0 = 0.0f0
	#return Float32[x[1],y[1],z[1],T0, x[2],y[2],z[2],T0, x[3],y[3],z[3],T0, 0,0,0,1]
	return Float32[
        x[1], y[1], z[1], T0,
        x[2], y[2], z[2], T0,
        x[3], y[3], z[3], T0,
        -dot(x,eyePos),-dot(y,eyePos),-dot(z,eyePos),1.0f0]
end

function perspectiveprojection(fovy, aspect, znear::T, zfar::T) where T
    h = T(tan(deg2rad(fovy)) * znear)
    w = T(h * aspect)
    bottom = -h
    top = h
    left = -w
    right = w
       
    (right == left || bottom == top || znear == zfar) && return eye4
    T0, T1, T2 = zero(T), one(T), T(2)
    return T[
        T2 * znear / (right - left), T0, T0, T0,
        T0, T2 * znear / (top - bottom), T0, T0,
        (right + left) / (right - left), (top + bottom) / (top - bottom), -(zfar + znear) / (zfar - znear), -T1,
        T0, T0, -(T2 * znear * zfar) / (zfar - znear), T0
    ]
end

function rotmat_x(angle)
	T0, T1 = 0.0f0, 1.0f0
	Float32[
		T1, T0, T0, T0,
		T0, cos(angle), sin(angle), T0, 
		T0, -sin(angle), cos(angle),  T0, 
		T0, T0, T0, T1
	]
end
function rotmat_y(angle)
	T0, T1 = 0.0f0, 1.0f0
	Float32[		
		cos(angle), T0, -sin(angle), T0,
		T0, T1, T0, T0,
		sin(angle), T0, cos(angle),  T0,
		T0, T0, T0, T1
	]
end
function rotmat_z(angle)
	T0, T1 = 0.0f0, 1.0f0
	Float32[
		cos(angle), sin(angle), T0, T0,
		-sin(angle), cos(angle),  T0, T0,
		T0, T0, T1, T0,
		T0, T0, T0, T1
	]
end
function movementMat(d)
	return Float32[1,0,0,d[1], 0,1,0,d[2], 0,0,1,d[3], 0,0,0,1]
end

view = glGetUniformLocation(program, "view")
viewMat = lookat([0.0, 0, 1.0], [0, 0, 0], [0, 1, 0])
move = glGetUniformLocation(program, "move")
moveMat = eye4
displacement=zeros(3)
pproj = glGetUniformLocation(program, "proj")
proj = perspectiveprojection(45f0, 800f0/600f0, 1f0, 10f0)

GLFW.SetKeyCallback(window, (_, button, _, action, _) -> begin
	if button==GLFW.KEY_S && action == GLFW.PRESS
		d=[-0.1,0.0,0]
		global displacement+=d
		global moveMat
		moveMat=movementMat(displacement)
	end
	if button==GLFW.KEY_F && action == GLFW.PRESS
		GLFW.make_fullscreen!(window)
	end
	if button==GLFW.KEY_W && action == GLFW.PRESS
		GLFW.make_windowed!(window)
	end
	if button==GLFW.KEY_Q && action == GLFW.PRESS
		GLFW.SetWindowShouldClose(window, true)
	end
	global viewMat
	divisions=12
	if button==GLFW.KEY_DOWN && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_x(-pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
	if button==GLFW.KEY_RIGHT && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_y(-pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
	if button==GLFW.KEY_UP && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_x(pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
	if button==GLFW.KEY_LEFT && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_y(pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
end)

while !GLFW.WindowShouldClose(window)
	glClear(GL_COLOR_BUFFER_BIT)
	# Render here
	glDrawArrays(GL_TRIANGLES, 0, length(data))
	#glDrawArrays(GL_LINES, 0, length(data))
	
	glUniformMatrix4fv(view,1,GL_FALSE,viewMat)
	glUniformMatrix4fv(move,1,GL_FALSE,moveMat)
	glUniformMatrix4fv(pproj,1,GL_FALSE,proj)
	
	GLFW.SwapBuffers(window)
	GLFW.PollEvents()
end
GLFW.DestroyWindow(window)
