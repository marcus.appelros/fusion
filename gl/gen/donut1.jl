using GLFW
using ModernGL
using GLAbstraction
using GeometryBasics
using Colors

# Define the parameters
donut_radius = 1.0
circle_radius = 0.2
num_circles = 20

# Initialize GLFW and create a window
GLFW.Init()
const window = GLFW.CreateWindow(800, 600, "Tokamak Coil Visualization")

# Set up the OpenGL context
GLFW.MakeContextCurrent(window)
GLAbstraction.GLContext()

# Define a function to create a circle mesh
function create_circle(radius, segments=32)
    vertices = Point3f0[]
    indices = Int[]
    for i in 1:segments
        angle = 2π * (i-1) / segments
        push!(vertices, Point3f0(radius * cos(angle), radius * sin(angle), 0.0))
        push!(indices, i)
    end
    push!(indices, 1)
    return GLAbstraction.mesh(vertices, indices)
end

# Create the circles
circles = [create_circle(circle_radius) for i in 1:num_circles]

# Main rendering loop
while !GLFW.WindowShouldClose(window)
    # Set up the viewport and clear the screen
    glViewport(0, 0, 800, 600)
    glClearColor(0.2, 0.3, 0.3, 1.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    # Draw the circles around the donut
    for i in 1:num_circles
        angle = 2π * (i-1) / num_circles
        x = donut_radius * cos(angle)
        y = donut_radius * sin(angle)
        GLAbstraction.translate(x, y, 0.0)
        GLAbstraction.render(circles[i], color=RGB(1.0, 0.5, 0.2))
    end

    # Swap buffers and poll events
    GLFW.SwapBuffers(window)
    GLFW.PollEvents()
end

# Clean up
GLFW.Terminate()

