function create_torus(donut_radius, circle_radius, num_circles)
    vertices = Float32[]
    #indices = []
	circsegs=100
    for i in 1:num_circles
        #theta = 2π * (i - 1) / num_circles
        theta = pi/num_circles+(i-1)*pi/(num_circles/2)
        #for j in 1:num_circles
        #    phi = 2π * (j - 1) / num_circles
	for j in 1:circsegs
            phi = 2π * (j - 1) / circsegs

            x = (donut_radius + circle_radius * cos(phi)) * cos(theta)
            y = (donut_radius + circle_radius * cos(phi)) * sin(theta)
            z = circle_radius * sin(phi)

            push!(vertices, x, y, z)
        end
    end

    #=for i in 1:num_circles
        for j in 1:num_circles
            next_i = mod(i, num_circles) + 1
            next_j = mod(j, num_circles) + 1

            push!(indices, (i - 1) * num_circles + j)
            push!(indices, (next_i - 1) * num_circles + j)
            push!(indices, (i - 1) * num_circles + next_j)

            push!(indices, (next_i - 1) * num_circles + j)
            push!(indices, (next_i - 1) * num_circles + next_j)
            push!(indices, (i - 1) * num_circles + next_j)
        end
    end=#

    return vertices#, indices
end
