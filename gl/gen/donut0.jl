using GLFW
using ModernGL
using GeometryBasics
using CSyntax

function init_glfw()
    if !GLFW.Init()
        error("Failed to initialize GLFW")
    end

    GLFW.WindowHint(GLFW.CONTEXT_VERSION_MAJOR, 3)
    GLFW.WindowHint(GLFW.CONTEXT_VERSION_MINOR, 3)
    GLFW.WindowHint(GLFW.OPENGL_PROFILE, GLFW.OPENGL_CORE_PROFILE)

    window = GLFW.CreateWindow(800, 600, "Tokamak Coil Visualization")
    if window == C_NULL
        error("Failed to create GLFW window")
    end

    GLFW.MakeContextCurrent(window)
    #glLoadGL()
    return window
end

function create_torus(donut_radius, circle_radius, num_circles)
    vertices = []
    indices = []

    for i in 1:num_circles
        theta = 2π * (i - 1) / num_circles
        for j in 1:num_circles
            phi = 2π * (j - 1) / num_circles

            x = (donut_radius + circle_radius * cos(phi)) * cos(theta)
            y = (donut_radius + circle_radius * cos(phi)) * sin(theta)
            z = circle_radius * sin(phi)

            push!(vertices, x, y, z)
        end
    end

    for i in 1:num_circles
        for j in 1:num_circles
            next_i = mod(i, num_circles) + 1
            next_j = mod(j, num_circles) + 1

            push!(indices, (i - 1) * num_circles + j)
            push!(indices, (next_i - 1) * num_circles + j)
            push!(indices, (i - 1) * num_circles + next_j)

            push!(indices, (next_i - 1) * num_circles + j)
            push!(indices, (next_i - 1) * num_circles + next_j)
            push!(indices, (i - 1) * num_circles + next_j)
        end
    end

    return vertices, indices
end

function main()
    window = init_glfw()

    donut_radius = 1.0
    circle_radius = 0.2
    num_circles = 32

    vertices, indices = create_torus(donut_radius, circle_radius, num_circles)

    vertex_shader_src = """
    #version 330 core
    layout (location = 0) in vec3 aPos;
    void main()
    {
        gl_Position = vec4(aPos, 1.0);
    }
    """

    fragment_shader_src = """
    #version 330 core
    out vec4 FragColor;
    void main()
    {
        FragColor = vec4(1.0, 0.5, 0.2, 1.0);
    }
    """

    shader_program = glCreateProgram()
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    #glShaderSource(vertex_shader, vertex_shader_src)
    glShaderSource(vertex_shader, 1, Ptr{GLchar}[pointer(vertex_shader_src)], C_NULL)
    #glCompileShader(vertex_shader)
    glShaderSource(fragment_shader,1, Ptr{GLchar}[pointer(fragment_shader_src)], C_NULL)
    glCompileShader(fragment_shader)
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, fragment_shader)
    glLinkProgram(shader_program)
    glDeleteShader(vertex_shader)
    glDeleteShader(fragment_shader)

    VAO = GLuint(0)
    VBO = GLuint(0)
    EBO = GLuint(0)
    @c glGenVertexArrays(1, &VAO)
    @c glGenBuffers(1, &VBO)
    @c glGenBuffers(1, &EBO)

    glBindVertexArray(VAO)

    glBindBuffer(GL_ARRAY_BUFFER, VBO)
    glBufferData(GL_ARRAY_BUFFER, length(vertices) * sizeof(GLfloat), vertices, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, length(indices) * sizeof(GLuint), indices, GL_STATIC_DRAW)

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), C_NULL)
    glEnableVertexAttribArray(0)

    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)

    while !GLFW.WindowShouldClose(window)
        glClear(GL_COLOR_BUFFER_BIT)
        glUseProgram(shader_program)
        glBindVertexArray(VAO)
        glDrawElements(GL_TRIANGLES, length(indices), GL_UNSIGNED_INT, C_NULL)
        GLFW.SwapBuffers(window)
        GLFW.PollEvents()
    end

    @c glDeleteVertexArrays(1, &VAO)
    @c glDeleteBuffers(1, &VBO)
    @c glDeleteBuffers(1, &EBO)

    GLFW.Terminate()
end

main()

