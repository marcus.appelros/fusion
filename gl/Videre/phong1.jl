using CSyntax
using StaticArrays

@static if Sys.isapple()
    const VERSION_MAJOR = 4
    const VERSION_MINOR = 1
end

include(joinpath(@__DIR__, "glutils.jl"))
include(joinpath(@__DIR__, "camera.jl"))
include("utils.jl")

# init window
width, height = 640, 480
window = startgl(width, height)

glEnable(GL_DEPTH_TEST)
glDepthFunc(GL_LESS)

# set camera
camera = PerspectiveCamera()
setposition!(camera, [0.0, 0.0, 2.0])

function upTetrah0()
	p0=[0,0.612,0];p1=[0.5,-0.204,0.289];p2=[-0.5,-0.204,0.289];p3=[0,-0.204,-0.577]
	data=GLfloat[ p0[1], p0[2], p0[3],
		p2[1],p2[2], p2[3],
		p1[1],p1[2], p1[3],

		p1[1],p1[2], p1[3],
		p2[1],p2[2], p2[3],
		p3[1], p3[2], p3[3],

		p2[1],p2[2], p2[3],
		p0[1], p0[2], p0[3],
		p3[1],p3[2],p3[3],

		p3[1], p3[2], p3[3],
		p0[1],p0[2],p0[3],
		p1[1],p1[2], p1[3] ]
	n0=cross(p2-p0,p1-p2);n1=cross(p2-p1,p3-p2);n2=cross(p0-p2,p3-p0);n3=cross(p0-p3,p1-p0)
	normals=GLfloat[ n0[1],n0[2],n0[3],
		n0[1],n0[2],n0[3],
		n0[1],n0[2],n0[3],
		
		n1[1],n1[2],n1[3],
		n1[1],n1[2],n1[3],
		n1[1],n1[2],n1[3],
		
		n2[1],n2[2],n2[3],
		n2[1],n2[2],n2[3],
		n2[1],n2[2],n2[3],
		
		n3[1],n3[2],n3[3],
		n3[1],n3[2],n3[3],
		n3[1],n3[2],n3[3] ]
	return data,normals
end

points,normals=upTetrah()

# vertex and normal

#= 
p0=[1,0,-1/sqrt(2)]/2;p1=[-1,0,-1/sqrt(2)]/2;p2=[0,1,1/sqrt(2)]/2;p3=[0,-1,1/sqrt(2)]/2
p0u=[0,0.612,0];p1u=[0.5,-0.204,0.289];p2u=[-0.5,-0.204,0.289];p3u=[0,-0.204,-0.577]

works
points = GLfloat[ 0.0, 0.5, 0.0,
                  0.5,-0.5, 0.0,
                 -0.5,-0.5, 0.0,
                 -0.5,-0.5, 0.0,
                 0.5,0.5,0.0,
                 0.0, 0.5, 0.0]

normals = GLfloat[0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0]
                  
points = GLfloat[ -0.5, 0.5, 0.0,
                  0.5,-0.5, 0.0,
                 -0.5,-0.5, 0.0,
                 
                 0.5,-0.5, 0.0,
                 0.5,0.5,0.0,
                 0.0, 0.5, 0.0,
                 
                 0.5,-0.5,0.0,
                 0.5,0.5,0.0,
                 0.7,0.0,-0.7
                 ]

normals = GLfloat[0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5]
                  
p0=[1,0,-1/sqrt(2)]/2;p1=[-1,0,-1/sqrt(2)]/2;p2=[0,1,1/sqrt(2)]/2;p3=[0,-1,1/sqrt(2)]/2
points = GLfloat[ p0[1], p0[2], p0[3],
                  p1[1],p1[2], p1[3],
                 p2[1],p2[2], p2[3],
                 
                 p1[1],p1[2], p1[3],
                 p2[1],p2[2], p2[3],
                 p3[1], p3[2], p3[3],
                 
                 p2[1],p2[2], p2[3],
                 p3[1], p3[2], p3[3],
                 p0[1],p0[2],p0[3],
                 
                 p3[1], p3[2], p3[3],
                 p0[1],p0[2],p0[3],
                 p1[1],p1[2], p1[3]
                 ]

normals = GLfloat[0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0, 0.0, 1.0,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5,
                  0.0,0.5,0.5]

=#

# create VBO
points_vbo = GLuint(0)
@c glGenBuffers(1, &points_vbo)
glBindBuffer(GL_ARRAY_BUFFER, points_vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW)

normal_vbo = GLuint(0)
@c glGenBuffers(1, &normal_vbo)
glBindBuffer(GL_ARRAY_BUFFER, normal_vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW)

# create VAO
vao = GLuint(0)
@c glGenVertexArrays(1, &vao)
glBindVertexArray(vao)
glBindBuffer(GL_ARRAY_BUFFER, points_vbo)
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)
glBindBuffer(GL_ARRAY_BUFFER, normal_vbo)
glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)
glEnableVertexAttribArray(0)
glEnableVertexAttribArray(1)

# load and compile shaders from file
vert_shader = createshader(joinpath(@__DIR__, "phong.vert"), GL_VERTEX_SHADER)
frag_shader = createshader(joinpath(@__DIR__, "phong.frag"), GL_FRAGMENT_SHADER)

# link program
shader_prog = createprogram(vert_shader, frag_shader)

model_loc = glGetUniformLocation(shader_prog, "model_mat")
view_loc = glGetUniformLocation(shader_prog, "view_mat")
proj_loc = glGetUniformLocation(shader_prog, "projection_mat")
glUseProgram(shader_prog)
model_mat = Matrix{GLfloat}(I, 4, 4)
glUniformMatrix4fv(model_loc, 1, GL_FALSE, model_mat)
glUniformMatrix4fv(view_loc, 1, GL_FALSE, get_view_matrix(camera))
glUniformMatrix4fv(proj_loc, 1, GL_FALSE, get_projective_matrix(window, camera))

GLFW.SetKeyCallback(window, (_, button, _, action, _) -> begin
	if button==GLFW.KEY_S && action == GLFW.PRESS
		#d=[-0.1,0.0,0]
		#global displacement+=d
		#global moveMat
		#moveMat=movementMat(displacement)
	end
	if button==GLFW.KEY_R && action == GLFW.PRESS
		resetcamera!(camera)
	end
	if button==GLFW.KEY_F && action == GLFW.PRESS
		GLFW.make_fullscreen!(window)			#TODO make alt+enter toggle fullscreen
	end
	if button==GLFW.KEY_G && action == GLFW.PRESS
		GLFW.make_windowed!(window)
	end
	if button==GLFW.KEY_H && action == GLFW.PRESS
		GLFW.SetWindowShouldClose(window, true)
	end
end)

# enable cull face
glDisable(GL_CULL_FACE) #slower but less buggy			#TODO fix so it can be enabled, low prio since graphics don't really need to be optimized
#glEnable(GL_CULL_FACE)
#glCullFace(GL_BACK)
#glFrontFace(GL_CW)
# set background color to gray
glClearColor(0.0, 0.0, 0.0, 1.0)

let
updatefps = FPSCounter()
# render
while !GLFW.WindowShouldClose(window)
    updatefps(window)
    # clear drawing surface
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glViewport(0, 0, GLFW.GetFramebufferSize(window)...)
    # drawing
    glUseProgram(shader_prog)
    glBindBuffer(GL_ARRAY_BUFFER, vao)
    model_mat[1,4] = sin(time())
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, model_mat)
    glDrawArrays(GL_TRIANGLES, 0, length(points))
    # check and call events
    GLFW.PollEvents()
    # move camera
    updatecamera!(window, camera)
    glUniformMatrix4fv(view_loc, 1, GL_FALSE, get_view_matrix(camera))
    # swap the buffers
    GLFW.SwapBuffers(window)
    #yield() #didnt work
end
end # let

GLFW.DestroyWindow(window)
