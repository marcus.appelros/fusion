include("../system.jl")

mutable struct BufferInfo
	name::String
	#data
	start::Int
	length::Int
	mode::UInt32
	color::Vector{Float32}
	locMat::Matrix{Float32}
end
BufferInfo(name::String)=BufferInfo(name,0,1,0,ones(4),Matrix{Float32}(I, 4, 4))
mutable struct GLSystem
	sys::System
	step::Int
	isRunning::Bool
	bufferInfos::Vector{BufferInfo}
	#pbis::Int #particle buffer info start. Always starts at 1. Add dict for other things.
end
GLSystem(sys::System)=GLSystem(sys,1,false,[])
