import GLFW
using ModernGL, LinearAlgebra, CSyntax

include("glutil.jl")

#GLFW.Init()

window = GLFW.CreateWindow(640, 480, "Hello Triangle")
GLFW.MakeContextCurrent(window)

createcontextinfo()

data0 = GLfloat[
	0.5, 0.0, -1/(2*sqrt(2)),
	-0.5, 0.0, -1/(2*sqrt(2)),
	0.0,0.5, 1/(2*sqrt(2)),
	0.5, 0.0, -1/(2*sqrt(2)),
	-0.5, 0.0, -1/(2*sqrt(2)),
	0,-0.5,1/(2*sqrt(2)),
	
]
data = GLfloat[
	-0.5f0, -0.5f0, -0.5f0,  0.0f0,  0.0f0, -1.0f0,
         0.5f0, -0.5f0, -0.5f0,  0.0f0,  0.0f0, -1.0f0,
         0.5f0,  0.5f0, -0.5f0,  0.0f0,  0.0f0, -1.0f0,
         0.5f0,  0.5f0, -0.5f0,  0.0f0,  0.0f0, -1.0f0,
        -0.5f0,  0.5f0, -0.5f0,  0.0f0,  0.0f0, -1.0f0,
        -0.5f0, -0.5f0, -0.5f0,  0.0f0,  0.0f0, -1.0f0,

        -0.5f0, -0.5f0,  0.5f0,  0.0f0,  0.0f0,  1.0f0,
         0.5f0, -0.5f0,  0.5f0,  0.0f0,  0.0f0,  1.0f0,
         0.5f0,  0.5f0,  0.5f0,  0.0f0,  0.0f0,  1.0f0,
         0.5f0,  0.5f0,  0.5f0,  0.0f0,  0.0f0,  1.0f0,
        -0.5f0,  0.5f0,  0.5f0,  0.0f0,  0.0f0,  1.0f0,
        -0.5f0, -0.5f0,  0.5f0,  0.0f0,  0.0f0,  1.0f0,

        -0.5f0,  0.5f0,  0.5f0, -1.0f0,  0.0f0,  0.0f0,
        -0.5f0,  0.5f0, -0.5f0, -1.0f0,  0.0f0,  0.0f0,
        -0.5f0, -0.5f0, -0.5f0, -1.0f0,  0.0f0,  0.0f0,
        -0.5f0, -0.5f0, -0.5f0, -1.0f0,  0.0f0,  0.0f0,
        -0.5f0, -0.5f0,  0.5f0, -1.0f0,  0.0f0,  0.0f0,
        -0.5f0,  0.5f0,  0.5f0, -1.0f0,  0.0f0,  0.0f0,

         0.5f0,  0.5f0,  0.5f0,  1.0f0,  0.0f0,  0.0f0,
         0.5f0,  0.5f0, -0.5f0,  1.0f0,  0.0f0,  0.0f0,
         0.5f0, -0.5f0, -0.5f0,  1.0f0,  0.0f0,  0.0f0,
         0.5f0, -0.5f0, -0.5f0,  1.0f0,  0.0f0,  0.0f0,
         0.5f0, -0.5f0,  0.5f0,  1.0f0,  0.0f0,  0.0f0,
         0.5f0,  0.5f0,  0.5f0,  1.0f0,  0.0f0,  0.0f0,

        -0.5f0, -0.5f0, -0.5f0,  0.0f0, -1.0f0,  0.0f0,
         0.5f0, -0.5f0, -0.5f0,  0.0f0, -1.0f0,  0.0f0,
         0.5f0, -0.5f0,  0.5f0,  0.0f0, -1.0f0,  0.0f0,
         0.5f0, -0.5f0,  0.5f0,  0.0f0, -1.0f0,  0.0f0,
        -0.5f0, -0.5f0,  0.5f0,  0.0f0, -1.0f0,  0.0f0,
        -0.5f0, -0.5f0, -0.5f0,  0.0f0, -1.0f0,  0.0f0,

        -0.5f0,  0.5f0, -0.5f0,  0.0f0,  1.0f0,  0.0f0,
         0.5f0,  0.5f0, -0.5f0,  0.0f0,  1.0f0,  0.0f0,
         0.5f0,  0.5f0,  0.5f0,  0.0f0,  1.0f0,  0.0f0,
         0.5f0,  0.5f0,  0.5f0,  0.0f0,  1.0f0,  0.0f0,
        -0.5f0,  0.5f0,  0.5f0,  0.0f0,  1.0f0,  0.0f0,
        -0.5f0,  0.5f0, -0.5f0,  0.0f0,  1.0f0,  0.0f0
       ]

#=vao = glGenVertexArray()
glBindVertexArray(vao)
vbo = glGenBuffer()
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW)
=#

vbo = GLuint(0)
@c glGenBuffers(1, &vbo)
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW)

vao = GLuint(0)
@c glGenVertexArrays(1, &vao)
glBindVertexArray(vao)
#glBindBuffer(GL_ARRAY_BUFFER, vbo)
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)
glEnableVertexAttribArray(0)

#=unsigned int VBO, cubeVAO;
    glGenVertexArrays(1, &cubeVAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(cubeVAO);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
=#

glEnable(GL_DEPTH_TEST)			#Could it be easy as adding this for depth testing?
glDepthFunc(GL_LESS)

const vsh = """
$(get_glsl_version_string())
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec3 FragPos;
out vec3 Normal;

//uniform mat4 model;
uniform mat4 view;
//uniform mat4 projection;

void main()
{
	//FragPos = vec3(model * vec4(aPos, 1.0));
	FragPos = vec3(vec4(aPos, 1.0));
	Normal = aNormal;  

	//gl_Position = projection * view * vec4(FragPos, 1.0);
	gl_Position = view * vec4(FragPos, 1.0);
}
"""
const fsh = """
$(get_glsl_version_string())
out vec4 FragColor;

in vec3 Normal;  
in vec3 FragPos;  
  
uniform vec3 lightPos; 
uniform vec3 lightColor;
uniform vec3 objectColor;

void main()
{
    // ambient
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;
  	
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
            
    vec3 result = (ambient + diffuse) * objectColor;
    FragColor = vec4(result, 1.0);
} 
"""

vertexShader = createShader(vsh, GL_VERTEX_SHADER)
fragmentShader = createShader(fsh, GL_FRAGMENT_SHADER)

program = glCreateProgram()
glAttachShader(program, vertexShader)
glAttachShader(program, fragmentShader)
glLinkProgram(program)

#glUseProgram(program)
#positionAttribute = glGetAttribLocation(program, "position");
#glEnableVertexAttribArray(positionAttribute)
#glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)

function lookat(eyePos, lookAt, up)
	z  = normalize(eyePos-lookAt)
	x  = normalize(cross(up, z))
	y  = normalize(cross(z, x))
	T0 = 0.0f0
	#return Float32[x[1],y[1],z[1],T0, x[2],y[2],z[2],T0, x[3],y[3],z[3],T0, 0,0,0,1]
	return Float32[
        x[1], y[1], z[1], T0,
        x[2], y[2], z[2], T0,
        x[3], y[3], z[3], T0,
        -dot(x,eyePos),-dot(y,eyePos),-dot(z,eyePos),1.0f0]
end

function rotmat_x(angle)
	T0, T1 = 0.0f0, 1.0f0
	Float32[
		T1, T0, T0, T0,
		T0, cos(angle), sin(angle), T0, 
		T0, -sin(angle), cos(angle),  T0, 
		T0, T0, T0, T1
	]
end
function rotmat_y(angle)
	T0, T1 = 0.0f0, 1.0f0
	Float32[		
		cos(angle), T0, -sin(angle), T0,
		T0, T1, T0, T0,
		sin(angle), T0, cos(angle),  T0,
		T0, T0, T0, T1
	]
end
function rotmat_z(angle)
	T0, T1 = 0.0f0, 1.0f0
	Float32[
		cos(angle), sin(angle), T0, T0,
		-sin(angle), cos(angle),  T0, T0,
		T0, T0, T1, T0,
		T0, T0, T0, T1
	]
end
function movementMat(d)
	return Float32[1,0,0,d[1], 0,1,0,d[2], 0,0,1,d[3], 0,0,0,1]
end
eye4=Float32[1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1]

view = glGetUniformLocation(program, "view")
viewMat = lookat([0.0, 0, 1.0], [0, 0, 0], [0, 1, 0])
#move = glGetUniformLocation(program, "move")
#moveMat = eye4
#displacement=zeros(3)
pobjectColor = glGetUniformLocation(program, "objectColor")
plightColor = glGetUniformLocation(program, "lightColor")
plightPos = glGetUniformLocation(program, "lightPos")

GLFW.SetKeyCallback(window, (_, button, _, action, _) -> begin
	if button==GLFW.KEY_S && action == GLFW.PRESS
		d=[-0.1,0.0,0]
		global displacement+=d
		global moveMat
		moveMat=movementMat(displacement)
	end
	if button==GLFW.KEY_F && action == GLFW.PRESS
		GLFW.make_fullscreen!(window)
	end
	if button==GLFW.KEY_W && action == GLFW.PRESS
		GLFW.make_windowed!(window)
	end
	if button==GLFW.KEY_Q && action == GLFW.PRESS
		GLFW.SetWindowShouldClose(window, true)
	end
	global viewMat
	divisions=12
	if button==GLFW.KEY_DOWN && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_x(-pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
	if button==GLFW.KEY_RIGHT && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_y(-pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
	if button==GLFW.KEY_UP && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_x(pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
	if button==GLFW.KEY_LEFT && action == GLFW.PRESS
		viewMat=reshape(reshape(rotmat_y(pi/divisions),4,4)*reshape(viewMat,4,4),16)
	end
end)
lastFrame=0.0
while !GLFW.WindowShouldClose(window)
	global lastFrame
	currentFrame=time()
	deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	# Render here
	glDrawArrays(GL_TRIANGLES, 0, length(data))
	#glDrawArrays(GL_LINES, 0, length(data))
	glUniform3f(pobjectColor,1.0f0, 0.5f0, 0.31f0)
	glUniform3f(plightColor,1.0f0, 1.0f0, 1.0f0)
	glUniform3f(plightPos,1.2f0, 1.0f0, 2.0f0)
	
	glUniformMatrix4fv(view,1,GL_FALSE,viewMat)
	#glUniformMatrix4fv(move,1,GL_FALSE,moveMat)
	
	GLFW.SwapBuffers(window)
	GLFW.PollEvents()
end
GLFW.DestroyWindow(window)
