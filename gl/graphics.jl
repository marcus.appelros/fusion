using GLFW, ModernGL
include("gen/create_torus.jl")
include("Videre/camera.jl")

#include("../tokamak0.jl")
include("glSystem.jl")


# whenever the window size changed (by OS or user resize) this callback function executes
function framebuffer_size_callback(window::GLFW.Window, width::Cint, height::Cint)
    # make sure the viewport matches the new window dimensions; note that width and
    # height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height)
end

# process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
function processInput(window::GLFW.Window)
    GLFW.GetKey(window, GLFW.KEY_ESCAPE) == GLFW.PRESS && GLFW.SetWindowShouldClose(window, true)
end

# misc. config
const SCR_WIDTH = 800
const SCR_HEIGHT = 600

const vertexShaderSource = """
	#version 330 core
	layout (location = 0) in vec3 aPos;
	uniform mat4 model, view, proj;
	void main(void)
	{
	    gl_Position = proj * view * model * vec4(aPos, 1.0);
	}"""

const fragmentShaderSource = """
	#version 330 core
	uniform vec4 color;
	out vec4 FragColor;
	void main(void)
	{
	    FragColor = color;
	}"""


function launch(sys::System)

	glsys=GLSystem(sys)

	GLFW.WindowHint(GLFW.CONTEXT_VERSION_MAJOR, 3)
	GLFW.WindowHint(GLFW.CONTEXT_VERSION_MINOR, 3)
	GLFW.WindowHint(GLFW.OPENGL_PROFILE, GLFW.OPENGL_CORE_PROFILE)
	GLFW.WindowHint(GLFW.OPENGL_FORWARD_COMPAT, GL_TRUE)

	# create window
	window = GLFW.CreateWindow(SCR_WIDTH, SCR_HEIGHT, "Fusion Reactor Simulator")
	window == C_NULL && error("Failed to create GLFW window.")
	GLFW.MakeContextCurrent(window)
	GLFW.SetFramebufferSizeCallback(window, framebuffer_size_callback)

	glEnable(GL_DEPTH_TEST)
	glDepthFunc(GL_LESS)

	# build and compile shader program
	vertexShader = glCreateShader(GL_VERTEX_SHADER)
	glShaderSource(vertexShader, 1, Ptr{GLchar}[pointer(vertexShaderSource)], C_NULL)
	glCompileShader(vertexShader)
	successRef = Ref{GLint}(-1)
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, successRef)
	if successRef[] != GL_TRUE
	    infoLog = Vector{GLchar}(512)
	    glGetShaderInfoLog(vertexShader, 512, C_NULL, infoLog)
	    error("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n", String(infoLog))
	end

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
	glShaderSource(fragmentShader, 1, Ptr{GLchar}[pointer(fragmentShaderSource)], C_NULL)
	glCompileShader(fragmentShader)
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, successRef)
	if successRef[] != GL_TRUE
	    infoLog = Vector{GLchar}(512)
	    glGetShaderInfoLog(fragmentShader, 512, C_NULL, infoLog)
	    error("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n", String(infoLog))
	end

	# link shaders
	shaderProgram = glCreateProgram()
	glAttachShader(shaderProgram, vertexShader)
	glAttachShader(shaderProgram, fragmentShader)
	glLinkProgram(shaderProgram)
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, successRef)
	if successRef[] != GL_TRUE
	    infoLog = Vector{GLchar}(512)
	    glGetProgramInfoLog(shaderProgram, 512, C_NULL, infoLog)
	    error("ERROR::SHADER::PROGRAM::LINKING_FAILED\n", String(infoLog))
	end
	glDeleteShader(vertexShader)
	glDeleteShader(fragmentShader)

	model_loc = glGetUniformLocation(shaderProgram, "model")
	view_loc = glGetUniformLocation(shaderProgram, "view")
	proj_loc = glGetUniformLocation(shaderProgram, "proj")
	color_loc = glGetUniformLocation(shaderProgram, "color")

	camera = PerspectiveCamera()
	setposition!(camera, [0.0, 0.0, 3.0])

	glUniformMatrix4fv(view_loc, 1, GL_FALSE, get_view_matrix(camera))
	glUniformMatrix4fv(proj_loc, 1, GL_FALSE, get_projective_matrix(window, camera))


	vertices=Float32[]
	buffi=0
	for i in eachindex(glsys.sys.particles)
		bi=BufferInfo("particle")
		bi.start=buffi
		ch=charge(glsys.sys.particles[i])
		if ch>0
			bi.color=[0,0,1,1]
		elseif ch<0
			bi.color=[1,0,0,1]
		else
			bi.color=[1,1,1,1]
		end
		bi.locMat[1,4]=glsys.sys.locHist[glsys.step][i][1]
		bi.locMat[2,4]=glsys.sys.locHist[glsys.step][i][2]
		bi.locMat[3,4]=glsys.sys.locHist[glsys.step][i][3]
		push!(glsys.bufferInfos,bi)
		buffi+=bi.length
		push!(vertices,0,0,0)
	end
	append!(vertices,create_torus(glsys.sys.components[1].r_inner+glsys.sys.components[1].r_coils,glsys.sys.components[1].r_coils,glsys.sys.components[1].num_coils))
	bi0=BufferInfo("tokamak")
	bi0.start=buffi
	bi0.length=length(vertices)/3
	bi0.mode=GL_LINES
	push!(glsys.bufferInfos,bi0)
	buffi+=bi0.length

	vboRef = Ref{GLuint}(0)
	vaoRef = Ref{GLuint}(0)
	glGenVertexArrays(1, vaoRef)
	glGenBuffers(1, vboRef)
	vao = vaoRef[]
	vbo = vboRef[]
	glBindVertexArray(vao)
	glBindBuffer(GL_ARRAY_BUFFER, vbo)
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW)
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), Ptr{Cvoid}(0))
	glEnableVertexAttribArray(0)

	# note that this is allowed, the call to glVertexAttribPointer registered VBO as
	# the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	# You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO,
	# but this rarely happens. Modifying other VAOs requires a call to glBindVertexArray anyways
	# so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	glBindVertexArray(0)

	# uncomment this call to draw in wireframe polygons.
	# glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

	#model_mat = Matrix{GLfloat}(I, 4, 4)

	function stepFrame()
		glsys.step+=1
		if glsys.step>length(glsys.sys.tHist)
			glsys.step=1
		end
		for i in eachindex(glsys.sys.particles)
			bi=glsys.bufferInfos[i]
			bi.locMat[1,4]=glsys.sys.locHist[glsys.step][i][1]
			bi.locMat[2,4]=glsys.sys.locHist[glsys.step][i][2]
			bi.locMat[3,4]=glsys.sys.locHist[glsys.step][i][3]
			
		end
	end

	GLFW.SetKeyCallback(window, (_, button, _, action, _) -> begin
		if button==GLFW.KEY_B && action == GLFW.PRESS
			glsys.isRunning = !glsys.isRunning
		end
		if button==GLFW.KEY_N && action == GLFW.PRESS
			stepFrame()
		end
		if button==GLFW.KEY_R && action == GLFW.PRESS
			resetcamera!(camera)
		end
		if button==GLFW.KEY_F && action == GLFW.PRESS
			GLFW.make_fullscreen!(window)			#TODO make alt+enter toggle fullscreen
		end
		if button==GLFW.KEY_G && action == GLFW.PRESS
			GLFW.make_windowed!(window)
		end
		if button==GLFW.KEY_H && action == GLFW.PRESS
			GLFW.SetWindowShouldClose(window, true)
		end
	end)

	# render loop
	while !GLFW.WindowShouldClose(window)
	    processInput(window)

	    # render
	    glClearColor(0.0, 0.0, 0.0, 1.0)
	    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

		# draw
		glUseProgram(shaderProgram)
	    glBindVertexArray(vao)
	    
	    for bi in glsys.bufferInfos
	    	glUniformMatrix4fv(model_loc, 1, GL_FALSE, bi.locMat)
	    	glUniform4f(color_loc,bi.color...)
	    	glDrawArrays(bi.mode, bi.start, bi.length)
	    end
	    
	    #glUniformMatrix4fv(model_loc, 1, GL_FALSE, model_mat)
	    
		#glDrawArrays(GL_LINES, 0, length(vertices))
		#glDrawArrays(GL_LINES, 2, 2)

	    # swap buffers and poll IO events
	    GLFW.SwapBuffers(window)
	    GLFW.PollEvents()
	    updatecamera!(window, camera)
	    glUniformMatrix4fv(view_loc, 1, GL_FALSE, get_view_matrix(camera))
	    glUniformMatrix4fv(proj_loc, 1, GL_FALSE, get_projective_matrix(window, camera))
	    if glsys.isRunning
	    	stepFrame()
	    end
	    yield()
	end

	# optional: de-allocate all resources once they've outlived their purpose
	glDeleteVertexArrays(1, vaoRef)
	glDeleteBuffers(1, vboRef)

	GLFW.DestroyWindow(window)

end
