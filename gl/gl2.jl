#credit: https://github.com/Gnimuc/Videre

using GLFW
using ModernGL
using CSyntax

# set up OpenGL context version
# it seems OpenGL 4.1 is the highest version supported by MacOS.
@static if Sys.isapple()
    const VERSION_MAJOR = 4
    const VERSION_MINOR = 1
end

@static if Sys.isapple()
    GLFW.WindowHint(GLFW.CONTEXT_VERSION_MAJOR, VERSION_MAJOR)
    GLFW.WindowHint(GLFW.CONTEXT_VERSION_MINOR, VERSION_MINOR)
    GLFW.WindowHint(GLFW.OPENGL_PROFILE, GLFW.OPENGL_CORE_PROFILE)
    GLFW.WindowHint(GLFW.OPENGL_FORWARD_COMPAT, GL_TRUE)
else
    GLFW.DefaultWindowHints()
end

# create window
window = GLFW.CreateWindow(640, 480, "Hello Triangle")
@assert window != C_NULL "could not open window with GLFW3."
GLFW.MakeContextCurrent(window)

# get version info
renderer = unsafe_string(glGetString(GL_RENDERER))
version = unsafe_string(glGetString(GL_VERSION))
@info "Renderder: $renderer"
@info "OpenGL version supported: $version"

# enable depth test
glEnable(GL_DEPTH_TEST)			#Could it be easy as adding this for depth testing?
glDepthFunc(GL_LESS)

# hard-coded shaders
const vert_source = """
#version 410 core
in vec3 vp;
void main(void)
{
    gl_Position = vec4(vp, 1.0);
}"""
const vert_source1 = """
#version 460

in vec3 position;

void main() {
gl_Position = vec4(position, 1);
}
"""

const frag_source = """
#version 410 core
out vec4 frag_colour;
void main(void)
{
    frag_colour = vec4(0.5, 0.0, 0.5, 1.0);
}"""
const frag_source1 = """
#version 460

out vec4 outColor;
void main() {
outColor = vec4(0.5, 0.0, 0.5, 1.0);
}
"""

# compile shaders
vert_shader = glCreateShader(GL_VERTEX_SHADER)
glShaderSource(vert_shader, 1, Ptr{GLchar}[pointer(vert_source)], C_NULL)
glCompileShader(vert_shader)
frag_shader = glCreateShader(GL_FRAGMENT_SHADER)
glShaderSource(frag_shader, 1, Ptr{GLchar}[pointer(frag_source)], C_NULL)
glCompileShader(frag_shader)

# create and link shader program
program = glCreateProgram()
glAttachShader(program, vert_shader)
glAttachShader(program, frag_shader)
glLinkProgram(program)

# vertex data
points = GLfloat[ 0.0,  0.5, 0.0,
                  0.5, -0.5, 0.0,
                 -0.5, -0.5, 0.0]

# create buffers located in the memory of graphic card
vbo = GLuint(0)
@c glGenBuffers(1, &vbo)
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW)

# create VAO
vao = GLuint(0)
@c glGenVertexArrays(1, &vao)
glBindVertexArray(vao)
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)
glEnableVertexAttribArray(0)

#=
positionAttribute = glGetAttribLocation(program, "position");
glEnableVertexAttribArray(positionAttribute)
glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, false, 0, C_NULL)

vao = glGenVertexArray() #what is difference to glGenVertexArrays?
glBindVertexArray(vao)
vbo = glGenBuffer()
glBindBuffer(GL_ARRAY_BUFFER, vbo)
glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW)
=#

# set background color to gray
glClearColor(0.2, 0.2, 0.2, 1.0)

GLFW.SetKeyCallback(window, (_, button, _, action, _) -> begin
	if button==GLFW.KEY_S && action == GLFW.PRESS
		println("Step.")
	end
	if button==GLFW.KEY_F && action == GLFW.PRESS
		GLFW.make_fullscreen!(window)
	end
	if button==GLFW.KEY_W && action == GLFW.PRESS
		GLFW.make_windowed!(window)
	end
	if button==GLFW.KEY_Q && action == GLFW.PRESS
		GLFW.SetWindowShouldClose(window, true)
	end
end)

# render
while !GLFW.WindowShouldClose(window)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glUseProgram(program)
    glBindVertexArray(vao)
    glDrawArrays(GL_TRIANGLES, 0, 3)
    # check and call events
    GLFW.PollEvents()
    # swap the buffers
    GLFW.SwapBuffers(window)
end

GLFW.DestroyWindow(window)

#=
while !GLFW.WindowShouldClose(window)
	glClear(GL_COLOR_BUFFER_BIT)
	# Render here
	#glDrawArrays(GL_TRIANGLES, 0, length(vertices))
	#vao.render()
	glDrawArrays(GL_TRIANGLES, 0, 9) #3 or 6?
	#glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0) #doesnt work

	#glDrawArrays(GL_LINES, 0, 6)
	#glDrawArrays(GL_POINTS, 0, 6)
	# Swap front and back buffers
	GLFW.SwapBuffers(window)

	# Poll for and process events
	GLFW.PollEvents()
end
GLFW.DestroyWindow(window)=#
