include("system.jl")

sys=System()
tok=Tokamak()
tok.I_coils=1e7
tok.I_plasma=1e6
push!(sys.components,tok)
#iloc=[tok.r_inner+tok.r_coils,0,-tok.r_coils]
iloc=[tok.r_inner+tok.r_coils,0,0]
iv=[0,1,0]
println("Magnetic field: ",magneticField(tok,0,iloc))
n=1000
for i in 1:n
	addParticle!(sys,Particle(1,0,0,iloc.+rand(3)/30,iv.+rand(3)/30))
end
for i in 1:n
	addParticle!(sys,Particle(0,0,2000,iloc.+rand(3)/30,iv.+rand(3)/30))
end
n=3000
sys.dt=0.01
step!(sys,n)

