using LinearAlgebra
include("math.jl")

# Constants
μ₀ = 4π * 10^-7  # Vacuum permeability (H/m)

# Function to compute the Biot-Savart law for a segment
function biot_savart_segment(r, r_prime, dl, I)
    r_diff = r - r_prime
    norm_r_diff = norm(r_diff)
    μ₀ / (4π) * I * cross(dl, r_diff) / norm_r_diff^3
end

# Function to compute the Biot-Savart law for a current loop
function biot_savart_loop(r, path, I)
    B = zeros(3)  # Initialize magnetic field vector
    for i in 1:length(path)-1
        r_prime = path[i]
        dl = path[i+1] - path[i]
        B += biot_savart_segment(r, r_prime, dl, I)
    end
    return B
end

# Function to generate a circular path
function generate_circular_path(radius, num_points)
    path = [radius * [cos(2π * i / num_points), sin(2π * i / num_points), 0.0] for i in 1:num_points]
    push!(path, path[1])  # Close the loop
    return path
end

# Example usage
function main()
    # Define the observation point r (in meters)
    r = [0.0, 0.0, 1.0]
    
    # Generate a circular path with radius 1m and 100 points
    radius = 1.0
    num_points = 100
    path = generate_circular_path(radius, num_points)
    
    # Current in the circuit (in amperes)
    I = 1.0
    
    # Calculate B(r)
    B = biot_savart_loop(r, path, I)
    
    println("B(r) = $B")
end

#main()

function B_coil(x,y,z,a,I)
	mu = 4*pi*10^(-7);
	r = sqrt(x^2+y^2); #%distance from z-axis

	#%central loop
	z3 = z;
	#=k = sqrt(4*r*a/(z3^2+(a+r)^2));
	if k>0.999999
		@warn "k is very close to 1, ellipke accuracy might be slightly off. You may be too close to a singularity."
		println("k x y z: ",k," ",x," ",y," ",z)
	end=#
	k_sq = 4*r*a/(z3^2+(a+r)^2);
	K,E = ellipke(k_sq);
	Bz3 = mu*I/(2*pi*sqrt(z3^2+(a+r)^2))*((a^2-z3^2-r^2)/(z3^2+(r-a)^2)*E+K);
	Br3 = mu*z3*I/(2*pi*r*sqrt(z3^2+(a+r)^2))*((z3^2+r^2+a^2)/(z3^2+(r-a)^2)*E-K);
	Bx3 = Br3*x/r;
	By3 = Br3*y/r;


	#%total magnetic field - output
	if (x==0)&&(y==0)
	    Bx=0;
	    By=0;
	    Bz = Bz3;
	else
	    Bx = Bx3;
	    By = By3;
	    Bz = Bz3;
	end
	return [Bx,By,Bz]
end
function B_plasma(x,y,z,a,I_plasma)

	mu = 4*pi*10^(-7);

	Bx=0; By=0; Bz=0;

		#%magnetic field of the plasma current
	sigma = a/3; #%parameter of the Gauss curve
	phi =  atan(y,x);
	#distance = sqrt( z^2 + (x-(a+b)*cos(phi))^2 + (y-(a+b)*sin(phi))^2 ); #%distance to centre of plasma ring
	#r_curr=b+a/2
	r_curr=a
	distance = sqrt( z^2 + (x-r_curr*cos(phi))^2 + (y-r_curr*sin(phi))^2 )
	
	if true# (distance>0.0001) #%JIK.
		println(I_plasma)
		I2_r_plasma = I_plasma*erf(distance/(sigma*sqrt(2)));
		println(I2_r_plasma)
		r = sqrt(x^2+y^2);
		#k = sqrt(4*r*(a+b)/(z^2+((a+b)+r)^2));
		#k_sq = 4*r*(a+b)/(z^2+((a+b)+r)^2);
		k_sq = 4*r*r_curr/(z^2+(r_curr+r)^2);
		K,E = ellipke(k_sq);
		#Bz_plasma = mu*I2_r_plasma/(2*pi*sqrt(z^2+((a+b)+r)^2))*(((a+b)^2-z^2-r^2)/(z^2+(r-(a+b))^2)*E+K);
		#Br_plasma = mu*z*I2_r_plasma/(2*pi*r*sqrt(z^2+(b+r)^2))*((z^2+r^2+(a+b)^2)/(z^2+(r-(a+b))^2)*E-K);
		Bz_plasma = mu*I2_r_plasma/(2*pi*sqrt(z^2+(r_curr+r)^2))*((r_curr^2-z^2-r^2)/(z^2+(r-r_curr)^2)*E+K);
		Br_plasma = mu*z*I2_r_plasma/(2*pi*r*sqrt(z^2+(r_curr+r)^2))*((z^2+r^2+r_curr^2)/(z^2+(r-r_curr)^2)*E-K);
		Bx_plasma = Br_plasma*x/r;
		By_plasma = Br_plasma*y/r;

	
		Bx = Bx+Bx_plasma;
		By = By+By_plasma;
		Bz = Bz+Bz_plasma;
		
	else
		println("Distance: ",distance)
	end
	if isnan(Bx)
		println("Nan1")
		Bx=0
	end
	if isnan(By)
		println("Nan2")
		By=0
	end
	if isnan(Bz)
		println("Nan3")
		Bz=0
	end
	
	return [Bx,By,Bz]
end

# Function to compute the magnetic field from a single loop
function loop_magnetic_field(x, y, z, distance, a, I)
    r = sqrt(x^2 + y^2)
    z1 = z + distance / 2
    k_sq = 4 * r * a / (z1^2 + (a + r)^2)
    K = ellipk(k_sq)
    E = ellipe(k_sq)
    
    Bz1 = μ₀ * I / (2π * sqrt(z1^2 + (a + r)^2)) * ((a^2 - z1^2 - r^2) / (z1^2 + (r - a)^2) * E + K)
    Br1 = μ₀ * z1 * I / (2π * r * sqrt(z1^2 + (a + r)^2)) * ((z1^2 + r^2 + a^2) / (z1^2 + (r - a)^2) * E - K)
    
    Bx1 = Br1 * x / r
    By1 = Br1 * y / r
    if x == 0 && y == 0
    	return [0,0,Bz1]
    end
    return [Bx1, By1, Bz1]
end


radius = 1.0
num_points = 100
path = generate_circular_path(radius, num_points)
I = 1_000_000
B(r)=biot_savart_loop(r,path,I)
B2(r)=B_coil(r...,radius,I)
B3(r)=B_plasma(r...,radius,I)
distance=0
B4(r)=loop_magnetic_field(r...,distance,radius,I)
#=
for dx in 0:0.1:0.9
	Bs1=Vector()
	Bs2=Vector()
	for d in 0:0.1:1
		push!(Bs1,B2([dx,0,d]))
		#push!(Bs2,B_coil(-dx,0,-d,1,I))
		push!(Bs2,B3([dx,0,d]))
	end
	dif=Bs1.-Bs2
	perc=norm(dif)/norm(Bs1)*100
	println(perc,"%")
end
x=Vector{Float64}()
y=Vector{Float64}()
for p in path
	push!(x,p[1])
	push!(y,p[2])
end
using Plots
plot(x,y)
=#



# Example usage
function main()
    x, y, z = 0.0, 0.0, 1.0  # Observation point
    distance = 1.0
    a = 0.5
    b = 0.5
    I1 = 1.0
    I2 = 1.0
    
    B3(r)=total_magnetic_field(r...,distance,a,b,I1,I2)
    
    Bx, By, Bz = total_magnetic_field(x, y, z, distance, a, b, I1, I2)
    
    println("Bx = $Bx, By = $By, Bz = $Bz")
end
