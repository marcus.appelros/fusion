include("../system.jl")
include("../utils.jl")
include("B1.jl")

sys=System()
sys.dt=0.01
simSecs=30
n=Int(round(simSecs/sys.dt))
p=Particle(1,0,0,[0.0,0,0],[1,1.0,0])
addParticle!(sys,p)

I1 = 80; #current in the solenoid
N1 = 1500; #number of windings
I1 = N1*I1;

#I2 = 100; #current in the central solenoid
I2=0
N2 = 8000; #number of windings
I2 = N2*I2;

dist = 10; #distance between solenoids
a = 1; #radius of each coil
#b = 4; #radius of central coil
b = 5

x=0;y=0;z=0

B0v = B1(x, y, z, dist, a, b, I1, I2)
B0=norm(B0v)

function mf(loc)
	B=B1(loc[2],loc[3],loc[1], dist, a, b, I1, I2)
	return [B[3],B[1],B[2]]./B0 #B=2 at x=1.825
end
sys.magneticField=(t,loc)->mf(loc)
sys.electricField=(t,loc)->[0,0,0]

step!(sys,n)

#l0,l1,l2=splitHist(sys.locHist,1,Int(round(length(sys.locHist)/1000)))
l0,l1,l2=splitHist(sys.locHist,1)
xmax,step=findmax(l0)
#mf([xmax,0,0]) # =45

#lines_in_3D(l0,l1,l2)

#=
fig=xyz()
plotField!(loc->mf(loc),0,4.9,0.98)
=#
