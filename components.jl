mutable struct Tokamak
	num_coils::Int
	r_inner::Float64 #radius of center hole
	r_coils::Float64
	I_coils::Float64
	I_plasma::Float64
end
Tokamak()=Tokamak(16,0.25,0.5,1e6,1e6)
function shield(tok::Tokamak,t,loc) #return true if a particle becomes stuck in the walls of the tokamak
	x,y,z=loc
	phi =  atan(y,x);
	r_curr=tok.r_inner+tok.r_coils
	distance = sqrt( z^2 + (x-r_curr*cos(phi))^2 + (y-r_curr*sin(phi))^2 ) #distance to centre of plasma ring, this is calculated in magneticField so optimization is possible at the cost of simplicity.
	return distance>=tok.r_coils
end
function magneticField(tok::Tokamak,t,loc)
	
	mu = 4*pi*10^(-7);
	
	x,y,z=loc

	Bx=0; By=0; Bz=0;

	#%magnetic field of the coils
	for i in 0:tok.num_coils-1
		theta = pi/tok.num_coils+i*pi/(tok.num_coils/2); #angle between the i-th coil and the x-axis
		
		if abs(sin(theta))>0.001
			r1_ = x/(cos(theta)-sin(theta)*tan(atan(y,x)-theta))-tok.r_coils-tok.r_inner
			r1 = sqrt(r1_^2+z^2)
			z1 = ((tok.r_inner+tok.r_coils)*cos(theta)+r1_*cos(theta)-x)/sin(theta)
		else
			r1_ = x-tok.r_inner-tok.r_coils
			r1 = sqrt(r1_^2+z^2)
			z1 = y #this causes the divergence to be nonzero but it should be an artifact because z1==y but z1 above does not depend on y.
		end
		k_sq = 4*r1*tok.r_coils/(z1^2+(tok.r_coils+r1)^2);

		K,E = ellipke(k_sq);

		Bz1_ = mu*tok.I_coils/(2*pi*sqrt(z1^2+(tok.r_coils+r1)^2))*((tok.r_coils^2-z1^2-r1^2)/(z1^2+(r1-tok.r_coils)^2)*E+K); #Bz1_ is the magnetic field in the coil frame
		Br1_ = mu*z1*tok.I_coils/(2*pi*r1*sqrt(z1^2+(tok.r_coils+r1)^2))*((z1^2+r1^2+tok.r_coils^2)/(z1^2+(r1-tok.r_coils)^2)*E-K); #Br1_ is the magnetic field in the cail frame

		Bx1 = -sin(theta)*Bz1_+Br1_*r1_/r1*cos(theta); #%normal coordinates
		By1 = cos(theta)*Bz1_ + sin(theta)*Br1_*r1_/r1; #%normal coordinates
		Bz1 = Br1_*z/r1; #%normal coordinates

		#%adding the field of a single coil to the total field
		Bx = Bx+Bx1;
		By = By+By1;
		Bz = Bz+Bz1;

		if abs(Bx)<(5e-12) #%JIK.
			Bx=0;
		end
		if abs(By)<(5e-12) #%JIK.
			By=0;
		end
		if abs(Bz)<(5e-12) #%JIK.
			Bz=0;
		end

	end


	#magnetic field of the plasma current
	phi =  atan(y,x);
	r_curr=tok.r_inner+tok.r_coils
	distance = sqrt( z^2 + (x-r_curr*cos(phi))^2 + (y-r_curr*sin(phi))^2 ) #distance to centre of plasma ring
	
	if (distance>0.0001) #%JIK.

		r = sqrt(x^2+y^2);
		k_sq = 4*r*r_curr/(z^2+(r_curr+r)^2);
		K,E = ellipke(k_sq);
		Bz_plasma = mu*tok.I_plasma/(2*pi*sqrt(z^2+(r_curr+r)^2))*((r_curr^2-z^2-r^2)/(z^2+(r-r_curr)^2)*E+K);
		Br_plasma = mu*z*tok.I_plasma/(2*pi*r*sqrt(z^2+(r_curr+r)^2))*((z^2+r^2+r_curr^2)/(z^2+(r-r_curr)^2)*E-K);
		Bx_plasma = Br_plasma*x/r;
		By_plasma = Br_plasma*y/r;

	
		Bx = Bx+Bx_plasma;
		By = By+By_plasma;
		Bz = Bz+Bz_plasma;
		
	else
		#println("Singularity. Distance: ",distance)
	end
	if isnan(Bx)
		Bx=0
	end
	if isnan(By)
		By=0
	end
	if isnan(Bz)
		Bz=0
	end
	
	return [Bx,By,Bz]
end
